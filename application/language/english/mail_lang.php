<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 20/06/2017
 * Time: 16:44
 */

$lang['mail_only_three_days_left_subject'] = 'Only three days left to complete the transaction';
$lang['mail_only_three_days_left_line_1'] = 'You have a product that wating for you in a cupboard right now, add it only left three days to get it';
$lang['mail_only_three_days_left_button'] = 'See the product';
$lang['mail_hello_you'] = 'Hello you,';

$lang['mail_unsubscribe_link_text'] = 'Unsubscribe';
$lang['mail_unsubscribe_phrase'] = 'You don\'t like these emails ? ';