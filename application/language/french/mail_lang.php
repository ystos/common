<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 23/06/2017
 * Time: 16:23
 */
$lang['mail_customer_service_speech'] = 'Nous sommes à votre disposition pour tout renseignement complémentaire sur vos ventes et achats sur Ystos.com';
$lang['mail_signature'] = 'L\'équipe Ystos';
$lang['mail_question_about_company'] = 'Une question sur tes échanges sur Ystos ?';
$lang['mail_check_faq'] = 'Consulte notre F.A.Q';


// Rate seller mail
$lang['mail_rate_seller_subject'] = 'Hey %1$s, note ton achat, ca te prend 5 secondes';
$lang['mail_rate_seller_hello_you'] = 'Hello %1$s,';
$lang['mail_rate_seller_line_1'] = 'Tu viens d\'acheter %1$s, donne une note sur ton achat pour guider les prochains acheteurs';
$lang['mail_rate_seller_button'] = 'Note ton achat';

// New message seller mail
$lang['mail_new_message_seller_subject'] = '%1$s, tu a un nouveau message';
$lang['mail_new_message_seller_title'] = 'Message laissé par %1$s';
$lang['mail_new_message_seller_hello_you'] = 'Hello <strong>%1$s,</strong>';
$lang['mail_new_message_seller_line_1'] = 'Vous avez reçu un message de %1$s concernant votre <strong>%2$s</strong> mis en vente sur Ystos.';
$lang['mail_new_message_seller_line_2'] = 'Nous vous invitons à consulter rapidement votre espace personnel.';
$lang['mail_new_message_seller_button'] = 'Je consulte mes messages';

// New message interested user mail
$lang['mail_new_message_interested_user_subject'] = '%1$s, tu a un nouveau message';
$lang['mail_new_message_interested_user_title'] = 'Message laissé par %1$s';
$lang['mail_new_message_interested_user_hello_you'] = 'Hello <strong>%1$s,</strong>';
$lang['mail_new_message_interested_user_line_1'] = 'Vous avez reçu un message de %1$s concernant le produit <strong>%2$s</strong> mis en vente sur Ystos.';
$lang['mail_new_message_interested_user_line_2'] = 'Nous vous invitons à consulter rapidement votre espace personnel.';
$lang['mail_new_message_interested_user_button'] = 'Je consulte mes messages';

// New message rejected offer
$lang['mail_rejected_offer_subject'] = '%1$s, ton offre a été refusé par le vendeur';
$lang['mail_rejected_offer_title'] = 'Le vendeur à refusé l\'offre pour le produit %1$s';
$lang['mail_rejected_offer_hello_you'] = 'Hello <strong>%1$s,</strong>';
$lang['mail_rejected_offer_line_1'] = 'Le vendeur à refusé l\'offre que tu a fais sur le produit <strong>%1$s</strong> mis en vente sur Ystos.';

// Mail Validation
$lang['mail_validation_subject'] = '%1$s, plus que une dernière étape';
$lang['mail_validation_title'] = 'Valide ton adresse cela prend 2 secondes';
$lang['mail_validation_hello_you'] = 'Hello <strong>%1$s,</strong>';
$lang['mail_validation_line_1'] = 'Felicitation !! Tu viens de t\'inscrire sur Ystos.com, il ne te reste plus que la dernière étape: Valider ton adresse mail. Pour cela clique sur le bouton ci-dessous.';
$lang['mail_validation_button'] = 'Valider l\'adresse mail';

// Mail Welcome
$lang['mail_welcome_subject'] = 'Bienvenue sur Ystos';
$lang['mail_welcome_title'] = '';
$lang['mail_welcome_hello_you'] = 'Salut <strong>%1$s,</strong>';
$lang['mail_welcome_line_1'] = 'Merci pour ton inscription sur <a href="ystos.com">Ystos.com</a>.';
$lang['mail_welcome_line_2'] = 'Mon nom est Jérémy Platon et je suis cofondateur d\'Ystos. Je souhaiterais prendre personnellement le temps de  t\'accueillir sur notre plateforme e-commerce.';
$lang['mail_welcome_line_3'] = 'Chez Ystos, nous offrons une plateforme spécialisée pour <strong>simplifier et sécuriser</strong> tes échanges de produits électroniques et high-tech. J\'espère donc que tes prochaines expériences d\'achat & vente seront (bien plus) satisfaisantes.';
$lang['mail_welcome_line_4'] = 'Mon quotidien, en tant que cofondateur et responsable e-commerce, est de travailler à proposer la meilleure expérience d\'achat et de vente possible. Et cela ne sera possible que grâce à tes  différents retours : quels outils pourraient davantage améliorer tes ventes et achats ?  N\'hésite pas à me répondre à cette adresse mail: <a href="mailto:jeremy.platon@ystos.com">jeremy.platon@ystos.com</a>';
$lang['mail_welcome_line_5'] = 'Merci beaucoup, j\'attends avec impatience tes retours.';
$lang['mail_welcome_line_6'] = 'Bien cordialement,';
$lang['mail_welcome_signature'] = 'L\'équipe co-fondatrice,<br> Jérémy & Etienne d\'Ystos';

// New Offer Seller
$lang['mail_new_offer_seller_subject'] = 'Offe acceptée...et maintenant ?';
$lang['mail_new_offer_seller_title'] = 'Offe acceptée...et maintenant ?';
$lang['mail_new_offer_seller_hello_you'] = 'Salut <strong>%1$s,</strong>';
$lang['mail_new_offer_seller_line_1'] = 'Félicitation ton article est maintenant réservé. Fixe avec l\'utilisateur un rendez-vous dans un lieu sûr pour procéder à l\'échange. Pour recevoir ton paiement, pense à demander le code alphanumérique de 6 caractères lors de l\'échange faisant office de monnaie d\'échange. ';
$lang['mail_new_offer_seller_line_2'] = 'Le numéro de téléphone de l\'acheteur est le suivant:';
$lang['mail_new_offer_seller_button'] = 'Voir les coordonnées';

// New Offer Buyer
$lang['mail_new_offer_buyer_subject'] = 'Félicitation: Offre acceptée !';
$lang['mail_new_offer_buyer_title'] = 'Félicitation: Offre acceptée !';
$lang['mail_new_offer_buyer_hello_you'] = 'Salut <strong>%1$s,</strong>';
$lang['mail_new_offer_buyer_line_1'] = 'Félicitation ton offre a été acceptée. Prends maintenant contact avec le vendeur pour fixer un rendez-vous dans un lieu sûr. Attention, ta monnaie d\'échange est le code à 6 caractères ci dessous.';
$lang['mail_new_offer_buyer_line_2'] = 'Transmet donc ce code au vendeur si tu souhaites acquérir l\'article. Si finalement l\'article ne te convient pas, décline poliment l\'offre et ne transmets pas ton code.';
$lang['mail_new_offer_buyer_line_3'] = 'Pour toutes questions supplémentaires: n\'hésite pas à nous adresser un mail avant ton échange à <a href="mailto:hello@ystos.com">hello@ystos.com</a>.';
$lang['mail_new_offer_buyer_line_4'] = 'Avec Ystos, les échanges sont totalement sécurisés. Tu n\'as donc plus besoin de retirer de l\'argent avant tes échanges.';
$lang['mail_new_offer_buyer_line_5'] = 'Le numéro du vendeur est le suivant:';
$lang['mail_new_offer_buyer_button'] = 'Voir les coordonnées';

// Mail Validation
$lang['forgot_password_subject'] = '%1$s, réinitialise ton mot de passe';
$lang['forgot_password_title'] = 'Réinitialise ton mot passe';
$lang['forgot_password_hello_you'] = 'Hello <strong>%1$s,</strong>';
$lang['forgot_password_line_1'] = 'Tu viens demander la réinitialisation de ton mot de passe. Pour finaliser clique sur le bouton ci-dessous';
$lang['forgot_password_button'] = 'Réinitialiser mon mot de passe';