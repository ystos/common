<?php
namespace Ystos\Common\Repository\Messaging;
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 13/06/2017
 * Time: 00:19
 */

class Filtered_Message_Repository extends \Doctrine\ORM\EntityRepository
{
    public function countMessage()
    {
        $qb = $this->createQueryBuilder('m')->select('COUNT(m)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}