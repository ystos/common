<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 11/08/2017
 * Time: 16:37
 */

namespace Ystos\Common\Repository;

use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Ystos\Common\Entity\Favorite;
use Ystos\Common\Entity\Messaging\Conversation;
use Ystos\Common\Entity\Notification\Config;
use Ystos\Common\Entity\Payment\Mangopay_Information;
use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use Ystos\Common\Entity\User;

class User_Repository extends \Doctrine\ORM\EntityRepository
{
    protected $CI;
    function __construct(EntityManager $em, ClassMetadata $class)
    {
        parent::__construct($em, $class);

        $this->CI =& get_instance();
        $this->CI->load->library('session');
    }

    function login($data)
    {
        /**
         * @var $user_data \Ystos\Common\Entity\User
         */
        $user_data = $this->findOneBy(array('email' => $data['email']));

        if ($user_data !== null) {

            if ($user_data->getStatus() === true) {
                $user_account_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\User_account');
                /**
                 * @var $user_account User_account
                 */
                $user_account = $user_account_repository->findOneBy(array('user' => $user_data));

                if ($user_account !== null) {

                    if (password_verify($data['password'], $user_account->getPassword())) {

                        $login_time = new \DateTime();
                        $token = $this->create_token($user_data->getId(), strval($login_time->getTimestamp()));

                        $user_account->setToken($token);

                        $user_data->setLastLogin($login_time);
                        $user_data->setLoginIp($_SERVER['REMOTE_ADDR']);

                        try {
                            $this->getEntityManager()->persist($user_data);
                            $this->getEntityManager()->persist($user_account);
                            $this->getEntityManager()->flush();

                            $user_data->setAccount($user_account);
                            return array(
                                'error' => false,
                                'user' => $user_data
                            );
                        } catch (\Doctrine\DBAL\DBALException $e) {
                            log_message('error', 'User | Cannot save login information :' . $e->GetMessage());
                            return array(
                                'error' => true,
                                'message' => 'Une erreur s\'est produite lors de la connexion'
                            );
                        }
                    } else {
                        return array(
                            'error' => true,
                            'message' => 'Identifiants incorrects. '
                        );
                    }
                } else {
                    return array(
                        'error' => true,
                        'message' => 'Identifiants incorrects. '
                    );
                }
            } else {
                return array(
                    'error' => true,
                    'message' => 'Votre compte n\'est pas actif.'
                );
            }
        } else {
            return array(
                'error' => true,
                'message' => 'Identifiants incorrects. '
            );
        }
    }

    public function register($email, $password, \Ystos\Common\Entity\User $user)
    {

        /**
         * @var $user_data \Ystos\Common\Entity\User
         */
        $user_data = $this->findOneBy(array('email' => $email, 'provider' => 'email'));

        if ($user_data === null) {

            $user_account = new User_account();

            $user_account->setSalt(sha1(uniqid()));
            $user_account->setPassword($this->encode_password($password, $user_account->getSalt()));

            $user->setRegistrationDate(new \DateTime());
            $user->setStatus(true);
            $user->setProvider('email');


            $notification_config = new Config();


            try {
                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();

                $user_account->setUser($user);


                $this->getEntityManager()->persist($user_account);
                $this->getEntityManager()->flush();

                $notification_config->setChannelId($user->getId());
                $notification_config->setUser($user);


                $this->getEntityManager()->persist($notification_config);
                $this->getEntityManager()->flush();
                return array(
                    'error' => false,
                    'user' => $user,
                    'message' => 'Votre inscription a bien été enregistrée. Il ne vous reste plus qu\'a valider votre adresse'
                );
            } catch (\Doctrine\DBAL\DBALException $e) {
                log_message('error', 'User | Cannot save login information :' . $e->GetMessage());
                return array(
                    'error' => true,
                    'message' => 'Une erreur s\'est produite lors de l\'enregistrement.'
                );
            }

        } else {
            return array(
                'error' => true,
                'message' => 'Un compte existe déjà avec cette adresse mail.'
            );
        }
    }

    /**
     * Add or check facebook login information in database
     * @param Facebook_User_account $facebook_user_account
     * @param \Ystos\Common\Entity\User $user
     * @return array
     */
    public function facebook_login(Facebook_User_account $facebook_user_account, \Ystos\Common\Entity\User &$user)
    {
        $now = new \DateTime();

        $facebook_account_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\Facebook_User_account');

        /**
         * @var $facebook_user_account_tmp Facebook_User_account
         */
        $facebook_user_account_tmp = $facebook_account_repository->findOneBy(array('provider_id' => $facebook_user_account->getProviderId()));

        if ($facebook_user_account_tmp !== null) {

            /**
             * @var $user \Ystos\Common\Entity\User
             */
            $user = $this->find($facebook_user_account_tmp->getUser());

            if ($user->getStatus() === true) {

                $user->setLastLogin($now);
                $user->setLoginIp($_SERVER['REMOTE_ADDR']);

                $facebook_user_account_tmp->setToken($facebook_user_account->getToken());

                try {
                    $this->getEntityManager()->persist($user);
                    $this->getEntityManager()->persist($facebook_user_account_tmp);

                    $this->getEntityManager()->flush();

                    $user->setAccount($facebook_user_account_tmp);

                    return array(
                        'error' => false,
                        'user' => $user->to_array()
                    );
                } catch (\Doctrine\DBAL\DBALException $e) {
                    log_message('error', 'User | Facebook : Cannot save login information :' . $e->GetMessage());
                    return array(
                        'error' => true,
                        'message' => 'Une erreur s\'est produite lors de la connexion facebook.'
                    );
                }
            } else {
                return array(
                    'error' => true,
                    'message' => 'Votre compte n\'est pas actif.'
                );
            }
        } else {
            $user->setStatus(true);
            $user->setRegistrationDate($now);
            $user->setProvider('facebook');

            try {


                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();

                $facebook_user_account->setUser($user);
                $this->getEntityManager()->persist($facebook_user_account);
                $this->getEntityManager()->flush();

                $user->setAccount($facebook_user_account);


                $notification_config = new \Ystos\Common\Entity\Notification\Config();
                $notification_config->setChannelId($user->getId());
                $notification_config->setUser($user);


                $this->getEntityManager()->persist($notification_config);
                $this->getEntityManager()->flush();
                try {
                    /**
                     * We add the product via the API, it will be registered in ElasticSearch like that
                     */
                    $http = new Client(['base_uri' => $this->CI->config->item('api_url')]);

                    $el = $http->request('POST', 'api/users/welcome', [
                        'form_params' => [
                            'user_id' => $user->getId(),
                        ]]);

                } catch (BadResponseException $e) {
                    $el = $e->getResponse();
                    $responseBodyAsString = $el->getBody()->getContents();
                    echo $responseBodyAsString;
                }
                return array(
                    'error' => false,
                    'user' => $user->to_array()
                );
            } catch (\Doctrine\DBAL\DBALException $e) {
                log_message('error', 'User | Facebook : Cannot save facebook information :' . $e->GetMessage());
                return array(
                    'error' => true,
                    'message' => 'Une erreur s\'est produite lors de la connexion facebook.'
                );
            }
        }
    }

    public function google_login(Google_User_account $google_user_account, \Ystos\Common\Entity\User &$user)
    {

        $now = new \DateTime();

        $google_account_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\Google_User_account');

        /**
         * @var $google_user_account_tmp Google_User_account
         */
        $google_user_account_tmp = $google_account_repository->findOneBy(array('provider_id' => $google_user_account->getProviderId()));


        if ($google_user_account_tmp !== null) {

            /**
             * @var $user \Ystos\Common\Entity\User
             */
            $user = $this->find($google_user_account_tmp->getUser());

            if ($user->getStatus() === true) {

                $user->setLastLogin($now);
                $user->setLoginIp($_SERVER['REMOTE_ADDR']);

                $google_user_account_tmp->setToken($google_user_account->getToken());

                try {
                    $this->getEntityManager()->persist($user);
                    $this->getEntityManager()->persist($google_user_account_tmp);

                    $this->getEntityManager()->flush();
                    return array(
                        'error' => false,
                        'user' => $user->to_array()
                    );
                } catch (\Doctrine\DBAL\DBALException $e) {
                    log_message('error', 'User | Google : Cannot save login information :' . $e->GetMessage());
                    return array(
                        'error' => true,
                        'message' => 'Une erreur s\'est produite lors de la connexion google.'
                    );
                }
            } else {
                return array(
                    'error' => true,
                    'message' => 'Votre compte n\'est pas actif.'
                );
            }
        } else {
            $user->setStatus(true);
            $user->setRegistrationDate($now);
            $user->setProvider('google');

            try {
                $this->getEntityManager()->persist($user);
                $this->getEntityManager()->flush();

                $google_user_account->setUser($user);
                $this->getEntityManager()->persist($google_user_account);
                $this->getEntityManager()->flush();
                return array(
                    'error' => false,
                    'user' => $user->to_array()
                );
            } catch (\Doctrine\DBAL\DBALException $e) {
                log_message('error', 'User | Google : Cannot save google information :' . $e->GetMessage());
                return array(
                    'error' => true,
                    'message' => 'Une erreur s\'est produite lors de la connexion google.'
                );
            }
        }
    }

    public function update(\Ystos\Common\Entity\User $user, $types){
        $response = array();
        foreach ($types as $type) {
            switch ($type) {
                case "profile":
                    if (isset($data['profile']['picture']) && !empty($data['profile']['picture'])) {
                        if (!rename($this->CI->config->item('base_path').'assets/medias/pictures/tmp/'.$user->getPicture(), $this->CI->config->item('base_path').'assets/medias/pictures/'.$user->getPicture())) {
                            //unset($user->getPicture());
                        }
                    }

                    try{
                        $this->getEntityManager()->persist($user);
                        $this->getEntityManager()->flush();

                        $response['profile'] = array(
                            'error' => false,
                            'message' => 'Votre profil a été mis à jour.'
                        );
                    }catch (\Doctrine\DBAL\DBALException $e) {
                        log_message('error', 'User | Update : Cannot update information:' . $e->GetMessage());
                        $response['profile'] = array(
                            'error' => true,
                            'message' => 'Impossible de modifier votre profil actuellement.'
                        );
                    }
                    break;
                case "email":
                    $user_tmp = $this->findOneBy(array('email' => $user->getEmail(), 'provider' => 'email'));

                    if($user_tmp === null){
                        try{
                            $this->getEntityManager()->persist($user);
                            $this->getEntityManager()->flush();

                            $response['email'] = array(
                                'error' => false,
                                'message' => 'Votre adresse mail a été changée.'
                            );
                        }catch (\Doctrine\DBAL\DBALException $e) {
                            log_message('error', 'User | Update : Cannot update email:' . $e->GetMessage());
                            $response['email'] = array(
                                'error' => true,
                                'message' => 'Impossible de modifier votre adresse mail actuellement.'
                            );
                        }
                    } else {
                        $response['email'] = array(
                            'error' => true,
                            'message' => 'Un compte existe déjà avec cette adresse mail.'
                        );
                    }
                    break;
                case "password":
                    $salt = sha1(uniqid());

                    $user->getAccount()->setPassword($this->encode_password($user->getAccount()->getPassword(), $salt));
                    $user->getAccount()->setSalt($salt);
                    $user->getAccount()->setPasswordUpdateDate(new \DateTime());

                    try{
                        $this->getEntityManager()->persist($user);
                        $this->getEntityManager()->flush();

                        $response['password'] = array(
                            'error' => false,
                            'message' => 'Votre mot de passe a été modifié.'
                        );
                    }catch (\Doctrine\DBAL\DBALException $e) {
                        log_message('error', 'User | Update : Cannot update password:' . $e->GetMessage());
                        $response['password'] = array(
                            'error' => true,
                            'message' => 'Impossible de modifier votre mot de passe actuellement.'
                        );
                    }
                    break;
                default:
                    $response['default'] = array(
                        'error' => true,
                        'message' => 'Une erreur s\'est produite.'
                    );
            }
        }

        /*
         * TODO To FIX
         * if($user !== null) {
            $this->CI->session->set_userdata(array('logged_user' => serialize($user)));
        }*/

        return $response;
    }

    public function token_verification(\Ystos\Common\Entity\User $user)
    {
        if ($user->getAccount()->getToken() == hash("sha256", $user->getId() . $_SERVER['REMOTE_ADDR'] . $user->getLastLogin()->getTimestamp())) {
            $user_account_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\User_account');
            /**
             * @var $user_account User_account
             */
            $user_account = $user_account_repository->findOneBy(array('user' => $user));

            if ($user_account !== null) {
                return $user->getAccount()->getToken() === $user_account->getToken();
            }
        }
        return false;
    }

    public function facebook_token_verification(\Ystos\Common\Entity\User $user)
    {
        $user_account_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\Facebook_User_account');
        /**
         * @var $user_account Facebook_User_account
         */
        $user_account = $user_account_repository->findOneBy(array('user' => $user));

        if ($user_account !== null) {
            return $user->getAccount()->getToken() === $user_account->getToken();
        }
        return false;
    }

    public function google_token_verification(\Ystos\Common\Entity\User $user)
    {
        $user_account_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\Google_User_account');
        /**
         * @var $user_account Google_User_account
         */
        $user_account = $user_account_repository->findOneBy(array('user' => $user));

        if ($user_account !== null) {
            return $user->getAccount()->getToken() === $user_account->getToken();
        }
        return false;
    }

    public function delete(User $user){

        $products = $this->getEntityManager()->getRepository('Ystos\Common\Entity\Product')->findBy(array('owner' => $user));

        foreach ($products as $product){
            $this->getEntityManager()->remove($product);
        }


        /**
         * @var $mangopay_information Mangopay_Information
         */
        $mangopay_information = $this->getEntityManager()->getRepository('Ystos\Common\Entity\Payment\Mangopay_Information')->findBy(array('user' => $user));

        $this->getEntityManager()->remove($mangopay_information);

        if($user->getProvider() === 'google'){
            $user_account = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\Google_User_account')->findOneBy(array('user'=>$user));
        }
        elseif($user->getProvider() === 'facebook'){
            $user_account = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\Facebook_User_account')->findOneBy(array('user'=>$user));
        }
        else{
            $user_account = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User\User_Account')->findOneBy(array('user'=>$user));
        }

        $this->getEntityManager()->remove($user_account);



        $conversations_user = $this->getEntityManager()->getRepository('Ystos\Common\Entity\Messaging\Conversation')->findBy(array('user'=>$user));

        foreach ($conversations_user as $conversation_user) {
            /**
             * @var $conversation_user Conversation
             */
            foreach ($conversation_user->getMessages() as $message) {
                $this->getEntityManager()->remove($message);
            }

            $this->getEntityManager()->remove($conversation_user);
        }


        $conversations_owner = $this->getEntityManager()->getRepository('Ystos\Common\Entity\Messaging\Conversation')->findBy(array('owner'=>$user));
        foreach ($conversations_owner as $conversation_owner) {
            /**
             * @var $conversation_owner Conversation
             */
            foreach ($conversation_owner->getMessages() as $message) {
                $this->getEntityManager()->remove($message);
            }
            $this->getEntityManager()->remove($conversation_owner);
        }

        /**
         * @var $favorites Favorite
         */
        $favorites = $this->getEntityManager()->getRepository('Ystos\Common\Entity\Favorite')->findBy(array('user'=>$user));

        foreach ($favorites as $favorite){
            $this->getEntityManager()->remove($favorite);
        }

        $this->getEntityManager()->flush();


    }

    public function count(array $criteria = [])
    {
        $qb = $this->createQueryBuilder('u')->select('COUNT(u)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /*
     * Password encode for database
     */
    private function encode_password($pwd, $salt)
    {
        return hash("sha256", $salt . hash("sha256", $salt . hash("sha256", $pwd)));
    }

    /*
 * Create token for logged user
 */
    private function create_token($user_id, $time)
    {
        return hash("sha256", $user_id . $_SERVER['REMOTE_ADDR'] . $time);
    }


}