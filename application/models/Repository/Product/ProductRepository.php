<?php

namespace Ystos\Common\Repository\Product;

require_once COMMONPATH . '/models/Entity/User/User.php';
require_once COMMONPATH . '/models/Entity/Product/Product.php';

require_once COMMONPATH . '/helpers/search_service_helper.php';

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Ystos\Common\Service\Search_Service;
use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\User;

/**
 * Created by PhpStorm.
 * User: piche
 * Date: 13/06/2017
 * Time: 00:19
 */

class ProductRepository extends \Doctrine\ORM\EntityRepository
{
    protected $CI;
    function __construct(EntityManager $em, ClassMetadata $class)
    {
        parent::__construct($em, $class);

        $this->CI =& get_instance();
        $this->CI->load->library('session');
    }

    public function countProducts()
    {
        $qb = $this->createQueryBuilder('p')->select('COUNT(p)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function countProductsByUser(User $user)
    {
        $qb = $this->createQueryBuilder('p')->select('COUNT(p)')->where('IDENTITY(p.owner) = :user')
            ->setParameter('user',$user->getId());

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function get_product_list(array $products_ids){
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->andWhere('p.id IN (:ids)')
            ->setParameter('ids', $products_ids);

        return $qb->getQuery()->getScalarResult();
    }

    public function get_product_list_filtered_by_category(array $products_ids, $category_id){
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->andWhere('p.id IN (:ids)')
            ->andWhere('IDENTITY(p.category) = :category_id')
            ->andWhere('p.active = 1')
            ->setParameter('ids', $products_ids)
            ->setParameter('category_id', $category_id);

        return $qb->getQuery()->getResult();
    }


    public function activate(Product $product){
        $product->setStatus(1);

        try{
            $this->getEntityManager()->persist($product);
            $this->getEntityManager()->flush();

            $search_service = new Search_Service();
            $search_service->add_product($product);
        } catch (\Doctrine\DBAL\DBALException $e) {
            log_message('error', 'Classified |  Cannot save the product :' . $e->GetMessage());
            return array(
                'error' => true,
                'message' => 'Une erreur s\'est produite lors de l\'enregistrement de votre annonce.'
            );
        }
    }

    /**
     * @param Product $product
     * @return array
     */
    public function add_tmp(Product $product){
        foreach ($product->getPictures() as $key => $value) {
            if (!rename($this->CI->config->item('base_path').'assets/medias/classifieds/tmp/'.$value, $this->CI->config->item('base_path').'assets/medias/classifieds/'.$value)) {
                unset($product->getPictures()[$key]);
                $this->create_thumbnail($value);
            }
            else{
                $this->create_thumbnail($value);
            }
        }

        try{

            $product = $this->getEntityManager()->merge($product);
            $this->getEntityManager()->flush();

            return array(
                'error' => false,
                'id' => $product->getId(),
                'message' => 'Votre annonce a bien été enregistrée'
            );
        } catch (\Doctrine\DBAL\DBALException $e) {
            log_message('error', 'Product |  Cannot save the product :' . $e->GetMessage());
            return array(
                'error' => true,
                'message' => 'Une erreur s\'est produite lors de l\'enregistrement de votre annonce.'
            );
        }
    }

    /**
     * @param Product $product
     * @return array
     */
    public function add(Product $product){
        foreach ($product->getPictures() as $key => $value) {
            if (!rename($this->CI->config->item('base_path').'assets/medias/classifieds/tmp/'.$value, $this->CI->config->item('base_path').'assets/medias/classifieds/'.$value)) {
                unset($product->getPictures()[$key]);
                $this->create_thumbnail($value);
            }
            else{
                $this->create_thumbnail($value);
            }
        }

        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();

        try{
            return array(
                'error' => false,
                'id' => $product->getId(),
                'message' => 'Votre annonce a bien été enregistrée'
            );
        } catch (\Doctrine\DBAL\DBALException $e) {
            log_message('error', 'Product |  Cannot save the product :' . $e->GetMessage());
            return array(
                'error' => true,
                'message' => 'Une erreur s\'est produite lors de l\'enregistrement de votre annonce.'
            );
        }
    }

    /**
     * @param Product $product
     * @return array
     */
    public function edit(Product $product){
        foreach ($product->getPictures() as $key => $value) {
            if(file_exists($this->CI->config->item('public_assets_path').'medias/classifieds/'.$value)){
                continue;
            }
            if (!rename($this->CI->config->item('public_assets_path').'medias/classifieds/tmp/'.$value, $this->CI->config->item('public_assets_path').'medias/classifieds/'.$value)) {
                unset($product->getPictures()[$key]);
            }
            else{
                $this->create_thumbnail($value);
            }
        }

        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();

        try{
            return array(
                'error' => false,
                'id' => $product->getId(),
                'message' => 'Votre annonce a bien été modifié'
            );
        } catch (\Doctrine\DBAL\DBALException $e) {
            log_message('error', 'Product |  Cannot save the product :' . $e->GetMessage());
            return array(
                'error' => true,
                'message' => 'Une erreur s\'est produite lors de l\'enregistrement de votre annonce.'
            );
        }
    }

    public function desactivate(Product $product,$status){
        $product->setStatus($status);

        try{
            $this->getEntityManager()->persist($product);
            $this->getEntityManager()->flush();

            $search_service = new Search_Service();
            $search_service->remove_product($product);
        } catch (\Doctrine\DBAL\DBALException $e) {
            log_message('error', 'Product |  Cannot save the product :' . $e->GetMessage());
            return array(
                'error' => true,
                'message' => 'Une erreur s\'est produite lors de l\'enregistrement de votre annonce.'
            );
        }
    }

    public function create_thumbnail($file_name){

        $this->CI->load->library('image_lib');

        $img_cfg['image_library'] = 'gd2';
        $img_cfg['source_image'] = $this->CI->config->item('public_assets_path').'medias/classifieds/'.$file_name;
        $img_cfg['maintain_ratio'] = TRUE;
        $img_cfg['create_thumb'] = TRUE;
        $img_cfg['thumb_marker'] = '';
        $img_cfg['new_image'] = $this->CI->config->item('public_assets_path').'medias/classifieds/thumbnail/'.$file_name;
        $img_cfg['width'] = 400;
        $img_cfg['quality'] = 80;
        $img_cfg['height'] = 400;

        $this->CI->image_lib->initialize($img_cfg);
        $this->CI->image_lib->resize();


    }


}