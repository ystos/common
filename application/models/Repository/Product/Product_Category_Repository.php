<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 11/08/2017
 * Time: 16:37
 */

namespace Ystos\Common\Repository\Product;


use Ystos\Common\Entity\Product\Category;

class Product_Category_Repository extends \Doctrine\ORM\EntityRepository
{
    public function get_all_categories($max_depth = -1) {

        $qb = $this->createQueryBuilder('pc');

        $qb->orderBy('pc.depth','DESC');

        if ($max_depth >= 0) {
            $qb->where("pc.depth <= :max_depth" );
            $qb->setParameter('max_depth', $max_depth);
        }

        $categories = array();
        $tmp = array();
        return $qb->getQuery()->getResult();

    }
}