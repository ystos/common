<?php

namespace Ystos\Common\Repository;

require_once COMMONPATH . '/models/Entity/User/User.php';

use Ystos\Common\Entity\User;

/**
 * Created by PhpStorm.
 * User: piche
 * Date: 13/06/2017
 * Time: 00:19
 */

class SaleRepository extends \Doctrine\ORM\EntityRepository
{
    public function countSales()
    {
        $qb = $this->createQueryBuilder('p')->select('COUNT(p)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function countSalesByUser(User $user)
    {
        $qb = $this->createQueryBuilder('s')->select('COUNT(s)')->where('IDENTITY(s.seller) = :user')
            ->setParameter('user',$user->getId());

        return $qb->getQuery()->getSingleScalarResult();
    }
}