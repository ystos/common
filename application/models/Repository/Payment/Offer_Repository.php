<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 11/08/2017
 * Time: 16:37
 */

namespace Ystos\Common\Repository\Payment;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Ystos\Common\Entity\Payment\Offer;
use Ystos\Common\Entity\Product;
use Ystos\Common\Entity\User;

class Offer_Repository extends \Doctrine\ORM\EntityRepository
{
    protected $CI;
    function __construct(EntityManager $em, ClassMetadata $class)
    {
        parent::__construct($em, $class);

        $this->CI =& get_instance();
        $this->CI->load->library('session');
    }

    function new_offer($user_id,$classified_id,$price) {


        $classified_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\Product\Product');

        /**
         * @var $classified Product\Product
         */
        $classified = $classified_repository->find($classified_id);


        $user_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User');

        $user = $user_repository->find($user_id);

        if ($classified) {
            if ($classified->getOwner()->getId() != $user_id) {
                /**
                 * @var $classified_offer \Ystos\Common\Entity\Payment\Offer
                 */
                $classified_offer = $this->findOneBy(array('user'=>$user, 'classified' => $classified)) ;
                // If it's the first try on the second chance
                if ($classified_offer === null || $classified_offer->getTest() === 1 || $price >= $classified->getMinPriceAfterComm()) {
                    if ($price >= $classified->getMinPriceAfterComm()) {
                        $expire = time() + (15*60); //Expire dans 15 minutes
                        $data = array(
                            "user_id" => $user_id,
                            "classified_id" => $classified_id,
                            "price" => $price,
                            "expire" => $expire
                        );
                        setNewOfferRequestToken($user_id,$classified_id,$price,$expire);
                        $this->CI->session->set_userdata('offer', serialize($data));
                        return array(
                            'error' => false,
                            'success' => true
                        );
                    } else {
                        $date = new \DateTime();

                        if($classified_offer !== null && $classified_offer->getTest() === 1 ){
                            $classified_offer->setTest(2);
                            $new_classified_offer = $classified_offer;
                        }
                        else{
                            $new_classified_offer = new \Ystos\Common\Entity\Payment\Offer();
                            $new_classified_offer->setClassified($classified);
                            $new_classified_offer->setUser($user);
                            $new_classified_offer->setDate($date);
                            $new_classified_offer->setPrice($price);
                            $new_classified_offer->setAccepted(false);
                            $new_classified_offer->setCompleted(false);

                            $new_classified_offer->setTest(1);
                        }


                        try {
                        $this->getEntityManager()->persist($new_classified_offer);
                        $this->getEntityManager()->flush();
                            return array(
                                'error' => false,
                                'success' => false
                            );
                        } catch (\Doctrine\DBAL\DBALException $e) {
                            log_message('error', 'Offer | Cannot save offer:' . $e->GetMessage());
                            return array(
                                'error' => true,
                                'message' => 'Une erreur s\'est produite lors de l\'enregistrement de votre offre.'
                            );
                        }
                    }
                } else {
                    return array(
                        "error" => true,
                        "message" => "Vous avez déjà fait une offre pour cette annonce."
                    );
                }
            } else {
                return array(
                    "error" => true,
                    "message" => "Vous ne pouvez pas faire d'offre pour cette annonce."
                );
            }
        }
        return array(
            'error' => true,
            'message' => 'Une erreur inconnue s\'est produite.'
        );
    }

    function add_offer($user_id,$classified_id,$price,$transaction) {
        $classified_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\Product\Product');

        /**
         * @var $classified Product\Product
         */
        $classified = $classified_repository->find($classified_id);

        if ($classified) {
            if ($classified->getOwner()->getId() != $user_id) {

                /**
                 * @var $classified_offer Offer
                 */
                $classified_offer = $this->findOneBy(array('user'=>$user_id, 'classified' => $classified)) ;

                if ($classified_offer === null) {

                    $user_repository = $this->getEntityManager()->getRepository('Ystos\Common\Entity\User');
                    /**
                     * @var $user User
                     */
                    $user = $user_repository->find($user_id);

                    $new_classified_offer = new Offer();
                    $new_classified_offer->setClassified($classified);
                    $new_classified_offer->setUser($user);
                }
                else {
                    $new_classified_offer = $classified_offer;
                }
                    if ($price >= $classified->getMinPriceAfterComm()) {
                        $possible_chars = "AZERTYUPQSDFGHJKLMWXCVBN23456789";
                        $nb_chars = strlen($possible_chars) - 1;

                        $code = '';

                        for($i=0; $i < 6; $i++)
                        {
                            $pos = mt_rand(0, $nb_chars);
                            $car = $possible_chars[$pos];
                            $code .= $car;
                        }

                        $transaction["code"] = $code;

                        try {
                            /**
                             * We add the product via the API, it will be registered in ElasticSearch like that
                             */
                            $http = new Client(['base_uri' => $this->CI->config->item('api_url')]);

                            $el = $http->request('GET', 'api/payment/calculate_commission', [
                                'query' => [
                                    'price' =>$price
                                ]]);

                        } catch (BadResponseException $e) {
                            $el = $e->getResponse();
                            $responseBodyAsString = $el->getBody()->getContents();
                            echo $responseBodyAsString;
                        }
                        $response = json_decode($el->getBody());

                        $commission_per = $response->commission_percentage;
                        $commission_val = $response->commission_value;
                        $transaction['commission_value'] = $commission_val;
                        $transaction['commission_percentage'] = $commission_per;

                        $date = new \DateTime();

                        $new_classified_offer->setDate($date);
                        $new_classified_offer->setPrice($price);
                        $new_classified_offer->setAccepted(FALSE);
                        $new_classified_offer->setTransaction($transaction);
                        $new_classified_offer->setCompleted(FALSE);
                        $new_classified_offer->setTest(FALSE);

                        try {
                            $this->getEntityManager()->persist($new_classified_offer);
                            $this->getEntityManager()->flush();
                            return array(
                                'error' => false,
                                'success' => true
                            );
                        } catch (\Doctrine\DBAL\DBALException $e) {
                            log_message('error', 'Offer | Cannot save offer:' . $e->GetMessage());
                            return array(
                                'error' => true,
                                'message' => 'Une erreur s\'est produite lors de l\'enregistrement de votre offre.'
                            );
                        }
                    } else {
                        return array(
                            'error' => true,
                            'message' => 'Une erreur inconnue s\'est produite.'
                        );
                    }
            } else {
                return array(
                    "error" => true,
                    "message" => "Vous ne pouvez pas faire d'offre pour cette annonce."
                );
            }
        }
        return array(
            'error' => true,
            'message' => 'Une erreur inconnue s\'est produite.'
        );
    }

    function get_user_accepted_sells(User $user){

        $qb = $this->createQueryBuilder('co');

        $classified_offers = $qb->where('co.accepted = :accepted')->innerJoin('co.classified', 'c', 'WITH', 'c.owner = :user')
            ->setParameter('user',$user)
            ->setParameter('accepted',TRUE)->getQuery()->getResult();

        return $classified_offers;
    }

    function get_user_accepted_purchases(User $user){
        $qb = $this->createQueryBuilder('co');

        $classified_offers = $qb->where('co.accepted = :accepted')
            ->andWhere('co.user = :user')
            ->setParameter('user',$user)
            ->setParameter('accepted',TRUE)->getQuery()->getResult();

        return $classified_offers;
    }

    function get_user_offers(User $user){


        $recept_classified_offers = $this->findBy(array('user'=>$user, 'completed' => false, 'accepted' => true)) ;
        $data = array(
            "receipt" => array(),
            "send" => array()
        );
        foreach ($recept_classified_offers as $classified_offer) {
            /**
             * @var $classified_offer Offer
             */
            $tmp = array(
                "classified_id" => $classified_offer->getClassified()->getId(),
                "user_id" => $classified_offer->getUser()->getId(),
                "phone" => $classified_offer->getClassified()->getOwner()->getPhone(),
                "date" => $classified_offer->getDate(),
                "price" => $classified_offer->getPrice(),
                "accepted" => $classified_offer->isAccepted(),
                "transaction" => $classified_offer->getTransaction(),
                "completed" => $classified_offer->isCompleted(),
                "title" => $classified_offer->getClassified()->getTitle()
            );

            $data["receipt"][] = $tmp;
        }

        $qb = $this->createQueryBuilder('co');

        $send_classified_offers = $qb->where('co.completed = 0')->andWhere('co.accepted = 1')->innerJoin('co.classified', 'c', 'WITH', 'c.owner = :user')
            ->setParameter('user',$user)
            ->getQuery()->getResult();

        foreach ($send_classified_offers as $classified_offer) {
            /**
             * @var $classified_offer Offer
             */
            $tmp = array(
                "classified_id" => $classified_offer->getClassified()->getId(),
                "user_id" => $classified_offer->getUser()->getId(),
                "phone" => $classified_offer->getUser()->getPhone(),
                "date" => $classified_offer->getDate(),
                "price" => $classified_offer->getPrice(),
                "accepted" => $classified_offer->isAccepted(),
                "transaction" => $classified_offer->getTransaction(),
                "completed" => $classified_offer->isCompleted(),
                "title" => $classified_offer->getClassified()->getTitle()
            );

            $data["send"][] = $tmp;
        }

        return $data;
    }

    function make_offer_completed($classified_id, $user_id){
        try {
            /**
             * @var $classified_offer Offer
             */
            $classified_offer = $this->findOneBy(array('classified' => $classified_id,'user'=>$user_id));
            $classified_offer->setCompleted(true);

            $this->getEntityManager()->persist($classified_offer);
            $this->getEntityManager()->flush();

            return true;
        } catch (\Doctrine\DBAL\DBALException $e) {
            log_message('error', 'Offer | Cannot complete the offer:' . $e->GetMessage());
            return false;
        }
    }

}