<?php
namespace Ystos\Common\Repository;
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 13/06/2017
 * Time: 00:19
 */

class MessageRepository extends \Doctrine\ORM\EntityRepository
{
    public function countMessage()
    {
        $qb = $this->createQueryBuilder('m')->select('COUNT(m)');

        return $qb->getQuery()->getSingleScalarResult();
    }
}