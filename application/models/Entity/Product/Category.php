<?php

namespace Ystos\Common\Entity\Product;

require_once COMMONPATH . 'models/Repository/Product/Category_Repository.php';

use Doctrine\Common\Collections\ArrayCollection;
use JsonSerializable;
/**
 * Category Model
 *
 * @Entity(repositoryClass="Ystos\Common\Repository\Product\Category_Repository")
 * @Table(name="product_categories")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Category implements JsonSerializable
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $name;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $slug;
    /**
     * One Category has Many Categories.
     * @OneToMany(targetEntity="Category", mappedBy="parent")
     */
    protected $sub_categories;
    /**
     * Many Categories have One Category.
     * @ManyToOne(targetEntity="Category", inversedBy="sub_categories")
     * @JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;
    /**
     * @Column(type="integer", nullable=false)
     */
    protected $depth;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getSubCategories()
    {
        return $this->sub_categories;
    }

    /**
     * @param mixed $sub_categories
     */
    public function setSubCategories($sub_categories)
    {
        $this->sub_categories = $sub_categories;
    }

    /**
     * @return Category
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param mixed $depth
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    public function jsonSerialize()
    {
        // TODO
        return array(
            'id' => $this->id,
        );
    }
}