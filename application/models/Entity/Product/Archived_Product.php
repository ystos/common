<?php

namespace Ystos\Common\Entity\Product;

require_once COMMONPATH . '/models/Entity/User/User.php';

use JsonSerializable;
use Ystos\Common\Entity\User;

/**
 * Archived Product Model
 *
 * @Entity
 * @Table(name="archived_products")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Archived_Product implements JsonSerializable
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     */
    protected $owner;
    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=false)
     */
    protected $publish_date;
    /**
     * This date is used to order the product when we search
     * It will be reset to the current date we a seller want to send the porduct to the top
     * @Column(type="datetime", nullable=false)
     */
    protected $order_date;
    /**
     * Store the picture filename array
     * @Column(type="array", nullable=true)
     */
    protected $pictures;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Product\Category")
     * @JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    protected $category;
    /**
     * @Column(type="string", length=200, nullable=false)
     */
    protected $title;
    /**
     * Store the characteristics array
     * @Column(type="array", nullable=true)
     */
    protected $characteristics;
    /**
     * @Column(type="text", nullable=true)
     */
    protected $description;
    /**
     * @Column(type="array", nullable=true)
     */
    protected $tags;
    /**
     * TODO Create a table with cities
     * @Column(type="string", nullable=false)
     */
    protected $city;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $longitude;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $latitude;
    /**
     * @Column(type="boolean", nullable=true)
     */
    protected $first_hand;
    /**
     * @Column(type="date", nullable=true)
     */
    protected $purchase_date;
    /**
     * @Column(type="boolean", nullable=true)
     */
    protected $warranty;
    /**
     * @Column(type="date", nullable=true)
     */
    protected $warranty_end_date;
    /**
     * 1 => active
     * 2 => wait for validation
     * 3 => disabled
     * 4 => sold
     * 5 => delivered
     * @Column(type="integer", nullable=true)
     */
    protected $product_state;
    /**
     * @Column(type="boolean", nullable=true)
     */
    protected $hand_delivery;
    /**
     * @Column(type="boolean", nullable=true)
     */
    protected $postal_delivery;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $price;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $min_price;
    /**
     * 1 => active
     * 2 => wait for validation
     * 3 => disabled
     * 4 => sold
     * 5 => delivered
     * @Column(type="integer", nullable=true)
     */
    protected $status;

    public function __construct(Product $product)
    {
        $this->id = $product->getId();
        $this->owner = $product->getOwner();
        $this->category = $product->getCategory();
        $this->title = $product->getTitle();
        $this->characteristics = $product->getCategory();
        $this->city = $product->getCity();
        $this->description = $product->getDescription();
        $this->first_hand = $product->isFirstHand();
        $this->hand_delivery = $product->isHandDelivery();
        $this->postal_delivery = $product->isPostalDelivery();
        $this->latitude = $product->getLatitude();
        $this->longitude = $product->getLongitude();
        $this->min_price = $product->getMinPrice();
        $this->price = $product->getPrice();
        $this->pictures = $product->getPictures();
        $this->product_state = $product->getProductState();
        $this->publish_date = $product->getPublishDate();
        $this->purchase_date = $product->getPurchaseDate();
        $this->status = $product->getStatus();
        $this->order_date = $product->getOrderDate();
        $this->warranty = $product->isWarranty();
        $this->warranty_end_date = $product->getWarrantyEndDate();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return \DateTime
     */
    public function getPublishDate()
    {
        return $this->publish_date;
    }

    /**
     * @param \DateTime $publish_date
     */
    public function setPublishDate($publish_date)
    {
        $this->publish_date = $publish_date;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->order_date;
    }

    /**
     * @param mixed $order_date
     */
    public function setOrderDate($order_date)
    {
        $this->order_date = $order_date;
    }

    /**
     * @return mixed
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * @param mixed $pictures
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
    }

    /**
     * @return \Ystos\Common\Entity\Product\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCharacteristics()
    {
        return $this->characteristics;
    }

    /**
     * @param mixed $characteristics
     */
    public function setCharacteristics($characteristics)
    {
        $this->characteristics = $characteristics;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return boolean
     */
    public function isFirstHand()
    {
        return $this->first_hand;
    }

    /**
     * @param mixed $first_hand
     */
    public function setFirstHand($first_hand)
    {
        $this->first_hand = $first_hand;
    }

    /**
     * @return mixed
     */
    public function getPurchaseDate()
    {
        return $this->purchase_date;
    }

    /**
     * @param mixed $purchase_date
     */
    public function setPurchaseDate($purchase_date)
    {
        $this->purchase_date = $purchase_date;
    }

    /**
     * @return mixed
     */
    public function isWarranty()
    {
        return $this->warranty;
    }

    /**
     * @param mixed $warranty
     */
    public function setWarranty($warranty)
    {
        $this->warranty = $warranty;
    }

    /**
     * @return mixed
     */
    public function getWarrantyEndDate()
    {
        return $this->warranty_end_date;
    }

    /**
     * @param mixed $warranty_end_date
     */
    public function setWarrantyEndDate($warranty_end_date)
    {
        $this->warranty_end_date = $warranty_end_date;
    }

    /**
     * @return mixed
     */
    public function getProductState()
    {
        return $this->product_state;
    }

    /**
     * @param mixed $product_state
     */
    public function setProductState($product_state)
    {
        $this->product_state = $product_state;
    }

    /**
     * @return mixed
     */
    public function isHandDelivery()
    {
        return $this->hand_delivery;
    }

    /**
     * @param mixed $hand_delivery
     */
    public function setHandDelivery($hand_delivery)
    {
        $this->hand_delivery = $hand_delivery;
    }

    /**
     * @return mixed
     */
    public function isPostalDelivery()
    {
        return $this->postal_delivery;
    }

    /**
     * @param mixed $postal_delivery
     */
    public function setPostalDelivery($postal_delivery)
    {
        $this->postal_delivery = $postal_delivery;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getMinPrice()
    {
        return $this->min_price;
    }

    /**
     * @param mixed $min_price
     */
    public function setMinPrice($min_price)
    {
        $this->min_price = $min_price;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    public function jsonSerialize()
    {
        // TODO
        return array(
            'id' => $this->id,
            'owner' => $this->owner,
            'category' => $this->category,
            'title' => $this->title,
            'description' => $this->description,
            'post_date' => $this->publish_date->format('d/m/Y H:m'),
            'min_price' => $this->min_price,
            'price' => $this->price,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
        );
    }

}