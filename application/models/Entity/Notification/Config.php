<?php

namespace Ystos\Common\Entity\Notification;


/**
 * Notification Config Model
 *
 * @Entity
 * @Table(name="notification_config")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Config
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @OneToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @Column(type="string",length=250, nullable=false)
     */
    protected $channel_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getChannelId()
    {
        return $this->channel_id;
    }

    /**
     * @param mixed $channel_id
     */
    public function setChannelId($user_id)
    {
        $time = new \DateTime();
        $channel_id = hash("sha256", $user_id . $_SERVER['REMOTE_ADDR'] . $time->getTimestamp());
        $this->channel_id = $channel_id;
    }




}