<?php

namespace Ystos\Common\Entity\Geo;


/**
 * Departement Model
 *
 * @Entity
 * @Table(name="geo_departement")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Departement
{
    /**
     * @Id
     * @Column(type="string", nullable=false)
     */
    protected $id;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $name;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $slug;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $longitude;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $latitude;
    /**
     * @var $region Region
     * @ManyToOne(targetEntity="Region")
     * @JoinColumn(name="region_id", referencedColumnName="id", nullable=false)
     */
    protected $region;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getRegion() : Region
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }


}