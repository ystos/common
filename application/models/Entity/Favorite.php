<?php

namespace Ystos\Common\Entity;

require_once COMMONPATH . 'models/Entity/User/User.php';
require_once COMMONPATH . 'models/Entity/Product/Product.php';

/**
 * Favorite Model
 *
 * @Entity
 * @Table(name="favorites", uniqueConstraints={@UniqueConstraint(name="favorite_unique", columns={"user_id", "product_id"})})
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Favorite
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Product\Product")
     * @JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    protected $product;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'product'=> $this->product->jsonSerialize(),
            'date' => $this->date,
        );
    }


}