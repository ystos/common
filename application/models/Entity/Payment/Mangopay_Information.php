<?php

namespace Ystos\Common\Entity\Payment;

use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\User;

require_once COMMONPATH . '/models/Entity/User/User.php';
require_once COMMONPATH . '/models/Entity/Product/Product.php';

/**
 * Mangopay_Information Model
 *
 * Store the information returned by mangopay
 *
 * @Entity
 * @Table(name="mangopay_information")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Mangopay_Information
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * The user
     * @OneToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * User Id provided by mangopay
     *
     * @Column(type="string", length=64,nullable=true)
     */
    protected $m_user_id;
    /**
     * Wallet Id provided by mangopay
     *
     * @Column(type="string", length=64,nullable=true)
     */
    protected $m_seller_wallet_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getMangopayUserId()
    {
        return $this->m_user_id;
    }

    /**
     * @param mixed $m_user_id
     */
    public function setMangopayUserId($m_user_id)
    {
        $this->m_user_id = $m_user_id;
    }

    /**
     * @return mixed
     */
    public function getMangopaySellerWalletId()
    {
        return $this->m_seller_wallet_id;
    }

    /**
     * @param mixed $m_seller_wallet_id
     */
    public function setMangopaySellerWalletId($m_seller_wallet_id)
    {
        $this->m_seller_wallet_id = $m_seller_wallet_id;
    }



    public function jsonSerialize()
    {
        // TODO
        return array(
            'id' => $this->id,
        );
    }


}