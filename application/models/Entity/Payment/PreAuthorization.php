<?php

namespace Ystos\Common\Entity\Payment;

use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\User;

require_once COMMONPATH . '/models/Entity/User/User.php';
require_once COMMONPATH . '/models/Entity/Product/Product.php';

/**
 * PreAuthorization Model
 *
 * @Entity
 * @Table(name="payment_pa")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class PreAuthorization
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * The preAuthorization id stored in MangoPay
     * @Column(type="integer", nullable=false)
     */
    protected $m_pa_id;
    /**
     * The user who will pay
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $buyer;
    /**
     * The product concern
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Product\Product")
     * @JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    protected $product;
    /**
     * The price payed by the buyer, because he can negotiate it
     * @Column(type="float", nullable=false)
     */
    protected $price_payed;
    /**
     * The status return by MangoPay
     * @Column(type="string", length=64, nullable=false)
     */
    protected $m_status;
    /**
     * The code value returned by MangoPay
     * @Column(type="string", length=64, nullable=false)
     */
    protected $value;
    /**
     * The expiration date return by Mangopay
     * @Column(type="datetime", nullable=false)
     */
    protected $m_expiration_date;
    /**
     * The Validation code provided for confirm the transaction
     * @Column(type="string", length=16, nullable=false)
     */
    protected $confirmation_code;
    /**
     * The Date of the order of the pre authorization
     * @Column(type="datetime", nullable=true)
     */
    protected $execution_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMangoPreAuthorizationId()
    {
        return $this->m_pa_id;
    }

    /**
     * @param mixed $m_pa_id
     */
    public function setMangoPreAuthorizationId($m_pa_id)
    {
        $this->m_pa_id = $m_pa_id;
    }

    /**
     * @return User
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param User $buyer
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getPricePayed()
    {
        return $this->price_payed;
    }

    /**
     * @param mixed $price_payed
     */
    public function setPricePayed($price_payed)
    {
        $this->price_payed = $price_payed;
    }


    /**
     * @return mixed
     */
    public function getMangoStatus()
    {
        return $this->m_status;
    }

    /**
     * @param mixed $m_status
     */
    public function setMangoStatus($m_status)
    {
        $this->m_status = $m_status;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getMangoExpirationDate()
    {
        return $this->m_expiration_date;
    }

    /**
     * @param mixed $m_expiration_date
     */
    public function setMangoExpirationDate($m_expiration_date)
    {
        $this->m_expiration_date = $m_expiration_date;
    }

    /**
     * @return mixed
     */
    public function getConfirmationCode()
    {
        return $this->confirmation_code;
    }

    /**
     * @param mixed $confirmation_code
     */
    public function setConfirmationCode($confirmation_code)
    {
        $this->confirmation_code = $confirmation_code;
    }

    /**
     * @return mixed
     */
    public function getMPaId()
    {
        return $this->m_pa_id;
    }

    /**
     * @param mixed $m_pa_id
     */
    public function setMPaId($m_pa_id)
    {
        $this->m_pa_id = $m_pa_id;
    }

    /**
     * @return mixed
     */
    public function getMStatus()
    {
        return $this->m_status;
    }

    /**
     * @param mixed $m_status
     */
    public function setMStatus($m_status)
    {
        $this->m_status = $m_status;
    }

    /**
     * @return mixed
     */
    public function getMExpirationDate()
    {
        return $this->m_expiration_date;
    }

    /**
     * @param mixed $m_expiration_date
     */
    public function setMExpirationDate($m_expiration_date)
    {
        $this->m_expiration_date = $m_expiration_date;
    }

    /**
     * @return mixed
     */
    public function getExecutionDate()
    {
        return $this->execution_date;
    }

    /**
     * @param mixed $execution_date
     */
    public function setExecutionDate($execution_date)
    {
        $this->execution_date = $execution_date;
    }



    public function jsonSerialize()
    {
        // TODO
        return array(
            'id' => $this->id,
            'mango_id' => $this->m_pa_id,
            'confirmation_code' => $this->confirmation_code,
        );
    }


}