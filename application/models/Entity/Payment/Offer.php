<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 18/08/2017
 * Time: 16:59
 */
namespace Ystos\Common\Entity\Payment;

require_once COMMONPATH . '/models/Entity/User/User.php';
require_once COMMONPATH . '/models/Entity/Product/Product.php';
require_once COMMONPATH . '/models/Repository/Payment/Offer_Repository.php';
/**
 * Offer Model
 *
 * @Entity(repositoryClass="Ystos\Common\Repository\Payment\Offer_Repository")
 * @Table(name="classified_offers")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Offer
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Product\Product")
     * @JoinColumn(name="classified_id", referencedColumnName="id", nullable=false)
     */
    protected $classified;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=false)
     */
    protected $date;
    /**
     * @Column(type="float", nullable=false)
     */
    protected $price;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $accepted;
    /**
     * @Column(type="integer", nullable=false)
     */
    protected $test;
    /**
     * @Column(type="array", nullable=true)
     */
    protected $transaction;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $completed;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $disabled = false;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getClassified()
    {
        return $this->classified;
    }

    /**
     * @param mixed $classified
     */
    public function setClassified($classified)
    {
        $this->classified = $classified;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function isAccepted()
    {
        return $this->accepted;
    }

    /**
     * @param mixed $accepted
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }

    /**
     * @return mixed
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * @param mixed $test
     */
    public function setTest($test)
    {
        $this->test = $test;
    }

    /**
     * @return mixed
     */
    public function getTransaction()
    {
        return $this->transaction;
    }

    /**
     * @param mixed $transaction
     */
    public function setTransaction($transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * @return mixed
     */
    public function isCompleted()
    {
        return $this->completed;
    }

    /**
     * @param mixed $completed
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

    /**
     * @return mixed
     */
    public function isDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param mixed $disabled
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }



    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'product'=> $this->product,
            'date' => $this->date,
        );
    }


}