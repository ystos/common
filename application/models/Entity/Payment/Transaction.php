<?php

namespace Ystos\Common\Entity\Payment;

require_once COMMONPATH . '/models/Entity/User/User.php';

/**
 * Transaction Model
 *
 * @Entity
 * @Table(name="transactions")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Transaction
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @Column(type="integer", length=64, nullable=false)
     */
    protected $id_product;
    /**
     * @Column(type="integer", nullable=false)
     */
    protected $value;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $date;
    /**
     * The response code of Mangopay
     * @Column(type="integer", nullable=false)
     */
    protected $response_code;
    /**
     * The identifier of the transaction on Mangopay.
     * @Column(type="integer", nullable=false)
     */
    protected $mangopay_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->id_product;
    }

    /**
     * @param mixed $id_product
     */
    public function setIdProduct($id_product)
    {
        $this->id_product = $id_product;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getResponseCode()
    {
        return $this->response_code;
    }

    /**
     * @param mixed $response_code
     */
    public function setResponseCode($response_code)
    {
        $this->response_code = $response_code;
    }

    /**
     * @return integer
     */
    public function getMangopayId()
    {
        return $this->mangopay_id;
    }

    /**
     * @param integer $mangopay_id
     */
    public function setMangopayId($mangopay_id)
    {
        $this->mangopay_id = $mangopay_id;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'id_product'=> $this->id_product,
            'date' => $this->date,
            'value' => $this->value,
            'response_code' => $this->response_code,
            'mangopay_id' => $this->mangopay_id,
        );
    }


}