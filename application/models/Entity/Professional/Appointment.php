<?php

namespace Ystos\Common\Entity\Professional;


require_once COMMONPATH . 'models/Entity/Professional/Shop.php';
require_once COMMONPATH . 'models/Entity/Professional/Service.php';
require_once COMMONPATH . 'models/Entity/User/User.php';

use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use phpDocumentor\Reflection\Types\Integer;


/**
 * Appointment Model
 *
 * @Entity
 * @Table(name="pro_appointment")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Appointment
{
    const STATUS_ACTIVE = 1;
    const STATUS_CANCELED = 0;
    const STATUS_DONE = 2;

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $date;
    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $end_date;
    /**
     * @ManyToOne(targetEntity="Service")
     * @JoinColumn(name="service_id", referencedColumnName="id")
     */
    protected $service;
    /**
     * Many Features have One Product.
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User", inversedBy="appointments")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $in_shop;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $in_home;
    /**
     * @var $create_date \DateTime | null
     * @Column(type="datetime", nullable=true)
     */
    protected $create_date;
    /**
     * @var $edit_date \DateTime | null
     * @Column(type="datetime", nullable=true)
     */
    protected $edit_date;
    /**
     * @Column(type="integer", nullable=false)
     */
    protected $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }



    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getInShop()
    {
        return $this->in_shop;
    }

    /**
     * @param mixed $in_shop
     */
    public function setInShop($in_shop)
    {
        $this->in_shop = $in_shop;
    }

    /**
     * @return mixed
     */
    public function getInHome()
    {
        return $this->in_home;
    }

    /**
     * @param mixed $in_home
     */
    public function setInHome($in_home)
    {
        $this->in_home = $in_home;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @param \DateTime|null $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * @return mixed
     */
    public function getEditDate()
    {
        return $this->edit_date;
    }

    /**
     * @param mixed $edit_date
     */
    public function setEditDate($edit_date)
    {
        $this->edit_date = $edit_date;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }



    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }

}