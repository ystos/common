<?php

namespace Ystos\Common\Entity\Professional;

require_once COMMONPATH . 'models/Entity/User/User.php';

/**
 * Config Model
 *
 * @Entity
 * @Table(name="pro_config")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Config
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $open_hour = "09:00";
    /**
     * @Column(type="string", nullable=false)
     */
    protected $close_hour = "18:00";
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $midday_close = true;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $midday_close_hour = "12:00";
    /**
     * @Column(type="string", nullable=true)
     */
    protected $midday_open_hour = "14:00";
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $auto_validation = false;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $logo;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $primary_color;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOpenHour()
    {
        return $this->open_hour;
    }

    /**
     * @param mixed $open_hour
     */
    public function setOpenHour($open_hour)
    {
        $this->open_hour = $open_hour;
    }

    /**
     * @return mixed
     */
    public function getCloseHour()
    {
        return $this->close_hour;
    }

    /**
     * @param mixed $close_hour
     */
    public function setCloseHour($close_hour)
    {
        $this->close_hour = $close_hour;
    }

    /**
     * @return mixed
     */
    public function getMiddayClose()
    {
        return $this->midday_close;
    }

    /**
     * @param mixed $midday_close
     */
    public function setMiddayClose($midday_close)
    {
        $this->midday_close = $midday_close;
    }

    /**
     * @return mixed
     */
    public function getMiddayCloseHour()
    {
        return $this->midday_close_hour;
    }

    /**
     * @param mixed $midday_close_hour
     */
    public function setMiddayCloseHour($midday_close_hour)
    {
        $this->midday_close_hour = $midday_close_hour;
    }

    /**
     * @return mixed
     */
    public function getMiddayOpenHour()
    {
        return $this->midday_open_hour;
    }

    /**
     * @param mixed $midday_open_hour
     */
    public function setMiddayOpenHour($midday_open_hour)
    {
        $this->midday_open_hour = $midday_open_hour;
    }

    /**
     * @return mixed
     */
    public function getAutoValidation()
    {
        return $this->auto_validation;
    }

    /**
     * @param mixed $auto_validation
     */
    public function setAutoValidation($auto_validation)
    {
        $this->auto_validation = $auto_validation;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return mixed
     */
    public function getPrimaryColor()
    {
        return $this->primary_color;
    }

    /**
     * @param mixed $primary_color
     */
    public function setPrimaryColor($primary_color)
    {
        $this->primary_color = $primary_color;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id
        );
    }


}