<?php

namespace Ystos\Common\Entity\Professional;


require_once COMMONPATH . 'models/Entity/Professional/Shop.php';

use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use phpDocumentor\Reflection\Types\Integer;


/**
 * Shop Model
 *
 * @Entity
 * @Table(name="pro_services")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Service
{
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * A shop can have many services
     * @ManyToOne(targetEntity="Shop", inversedBy="services")
     * @JoinColumn(name="shop_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $shop;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $name;
    /**
     * @Column(type="text", nullable=true)
     */
    protected $description;
    /**
     * @Column(type="float", nullable=false)
     */
    protected $price;
    /**
     * Estimated time for doing the tasks
     * @Column(type="integer", nullable=false)
     */
    protected $estimated_time;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $status;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $in_shop;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $in_home;
    /**
     * @var $publish_date \DateTime
     * @Column(type="datetime", nullable=false)
     */
    protected $publish_date;
    /**
     * @var $edit_date \DateTime | null
     * @Column(type="datetime", nullable=true)
     */
    protected $edit_date;
    /**
     * Store the picture filename array
     * @Column(type="array", nullable=true)
     */
    protected $pictures;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getEstimatedTime()
    {
        return $this->estimated_time;
    }

    /**
     * @param mixed $estimated_time
     */
    public function setEstimatedTime($estimated_time)
    {
        $this->estimated_time = $estimated_time;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getInShop()
    {
        return $this->in_shop;
    }

    /**
     * @param mixed $in_shop
     */
    public function setInShop($in_shop)
    {
        $this->in_shop = $in_shop;
    }

    /**
     * @return mixed
     */
    public function getInHome()
    {
        return $this->in_home;
    }

    /**
     * @param mixed $in_home
     */
    public function setInHome($in_home)
    {
        $this->in_home = $in_home;
    }

    /**
     * @return \DateTime
     */
    public function getPublishDate(): \DateTime
    {
        return $this->publish_date;
    }

    /**
     * @param \DateTime $publish_date
     */
    public function setPublishDate(\DateTime $publish_date)
    {
        $this->publish_date = $publish_date;
    }

    /**
     * @return \DateTime |null
     */
    public function getEditDate()
    {
        return $this->edit_date;
    }

    /**
     * @param \DateTime $edit_date
     */
    public function setEditDate(\DateTime $edit_date)
    {
        $this->edit_date = $edit_date;
    }

    /**
     * @return mixed
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * @param mixed $pictures
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
    }


    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }

}