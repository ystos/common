<?php

namespace Ystos\Common\Entity\Professional;


require_once COMMONPATH . 'models/Entity/Professional/Shop.php';
require_once COMMONPATH . 'models/Entity/User/User.php';

use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use phpDocumentor\Reflection\Types\Integer;


/**
 * Invitation Model
 *
 * @Entity
 * @Table(name="pro_team_member_invitation")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Invitation
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * A shop can have many services
     * @ManyToOne(targetEntity="Shop")
     * @JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $email;
    /**
     * @var $added_date \DateTime
     * @Column(type="datetime", nullable=false)
     */
    protected $date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }

}