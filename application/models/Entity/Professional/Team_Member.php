<?php

namespace Ystos\Common\Entity\Professional;


require_once COMMONPATH . 'models/Entity/Professional/Shop.php';
require_once COMMONPATH . 'models/Entity/User/User.php';

use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use phpDocumentor\Reflection\Types\Integer;


/**
 * Team Member Model
 *
 * @Entity
 * @Table(name="pro_team_member")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Team_Member
{
    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_BASIC = 3;

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * A shop can have many services
     * @ManyToOne(targetEntity="Shop", inversedBy="team_members")
     * @JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
    /**
     * A shop can have many services
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User", inversedBy="services")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @Column(type="integer", nullable=false)
     */
    protected $role;
    /**
     * @var $added_date \DateTime
     * @Column(type="datetime", nullable=false)
     */
    protected $added_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getAddedDate()
    {
        return $this->added_date;
    }

    /**
     * @param mixed $added_date
     */
    public function setAddedDate($added_date)
    {
        $this->added_date = $added_date;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }

}