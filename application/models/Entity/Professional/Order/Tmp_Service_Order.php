<?php

namespace Ystos\Common\Entity\Professional\Order;


require_once COMMONPATH . 'models/Entity/Professional/Shop.php';
require_once COMMONPATH . 'models/Entity/Professional/Service.php';
require_once COMMONPATH . 'models/Entity/User/User.php';

use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use phpDocumentor\Reflection\Types\Integer;


/**
 * Temporary service order Model
 *
 * @Entity
 * @Table(name="pro_tmp_service_order")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Tmp_Service_Order
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $start_date;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $end_date;
    /**
     * @OneToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Professional\Service")
     * @JoinColumn(name="service_id", referencedColumnName="id")
     */
    protected $service;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $in_shop;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $in_home;
    /**
     * @var $create_date \DateTime | null
     * @Column(type="datetime", nullable=true)
     */
    protected $create_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param mixed $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param mixed $end_date
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getInShop()
    {
        return $this->in_shop;
    }

    /**
     * @param mixed $in_shop
     */
    public function setInShop($in_shop)
    {
        $this->in_shop = $in_shop;
    }

    /**
     * @return mixed
     */
    public function getInHome()
    {
        return $this->in_home;
    }

    /**
     * @param mixed $in_home
     */
    public function setInHome($in_home)
    {
        $this->in_home = $in_home;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * @param \DateTime|null $create_date
     */
    public function setCreateDate($create_date)
    {
        $this->create_date = $create_date;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }

}