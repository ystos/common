<?php

namespace Ystos\Common\Entity\Professional;

require_once COMMONPATH . 'models/Entity/User/User.php';
require_once COMMONPATH . 'models/Entity/Professional/Service.php';
require_once COMMONPATH . 'models/Entity/Professional/Config.php';
require_once COMMONPATH . 'models/Entity/Professional/Legal_Information.php';
require_once COMMONPATH . 'models/Entity/Professional/Team_Member.php';
require_once COMMONPATH . 'models/Entity/Utils/Address.php';

use Doctrine\Common\Collections\ArrayCollection;
use Ystos\Common\Entity\User;
use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use phpDocumentor\Reflection\Types\Integer;


/**
 * Shop Model
 *
 * @Entity
 * @Table(name="pro_shops")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Shop
{

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $name;
    /**
     *
     * @OneToOne(targetEntity="Ystos\Common\Entity\Utils\Address", cascade={"persist"})
     * @JoinColumn(name="address_id", referencedColumnName="id", nullable=true)
     */
    private $address;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $email;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $phone;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $status;
    /**
     * @var $registration_date \DateTime
     * @Column(type="datetime", nullable=false)
     */
    protected $registration_date;
    /**
     * @var $last_login \DateTime
     * @Column(type="datetime", nullable=true)
     */
    protected $last_login;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $login_ip;
    /**
     * Store the picture filename array
     * @Column(type="array", nullable=true)
     */
    protected $pictures;
    /**
     * @Column(type="text", nullable=true)
     */
    protected $description;
    /**
     * @OneToMany(targetEntity="Ystos\Common\Entity\Professional\Service", mappedBy="shop")
     */
    protected $services;
    /**
     * @OneToMany(targetEntity="Ystos\Common\Entity\Professional\Team_Member", mappedBy="shop")
     */
    protected $team_members;

    /**
     * @OneToOne(targetEntity="Ystos\Common\Entity\Professional\Config")
     * @JoinColumn(name="config_id", referencedColumnName="id")
     */
    protected $config;

    /**
     * Stored in Redis
     * Sells counter
     * @var $sells_counter Integer | null
     */
    protected $sells_counter;
    /**
     * Stored in Redis
     * Purchases counter
     * @var $purchases_counter Integer | null
     */
    protected $purchases_counter;

    public function __construct() {
        $this->services = new ArrayCollection();
        $this->team_members = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getRegistrationDate(): \DateTime
    {
        return $this->registration_date;
    }

    /**
     * @param \DateTime $registration_date
     */
    public function setRegistrationDate(\DateTime $registration_date)
    {
        $this->registration_date = $registration_date;
    }

    /**
     * @return \DateTime
     */
    public function getLastLogin(): \DateTime
    {
        return $this->last_login;
    }

    /**
     * @param \DateTime $last_login
     */
    public function setLastLogin(\DateTime $last_login)
    {
        $this->last_login = $last_login;
    }

    /**
     * @return mixed
     */
    public function getLoginIp()
    {
        return $this->login_ip;
    }

    /**
     * @param mixed $login_ip
     */
    public function setLoginIp($login_ip)
    {
        $this->login_ip = $login_ip;
    }

    /**
     * @return mixed
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * @param mixed $pictures
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param mixed $services
     */
    public function setServices($services)
    {
        $this->services = $services;
    }

    public function addService(Service $service){
        $this->services->add($service);
    }

    public function removeService(Service $service){
        $this->services->removeElement($service);
    }

    /**
     * @return mixed
     */
    public function getTeamMembers()
    {
        return $this->team_members;
    }

    /**
     * @param mixed $team_members
     */
    public function setTeamMembers($team_members)
    {
        $this->team_members = $team_members;
    }


   public function addTeamMember(Team_Member $team_member){
        $this->team_members->add($team_member);
    }

    public function removeTeamMember(Team_Member $team_member){
        $this->team_members->removeElement($team_member);
    }

    /**
     * @return int | null
     */
    public function getSellsCounter()
    {
        return $this->sells_counter;
    }

    /**
     * @param int $sells_counter
     */
    public function setSellsCounter(int $sells_counter)
    {
        $this->sells_counter = $sells_counter;
    }

    /**
     * @return int | null
     */
    public function getPurchasesCounter()
    {
        return $this->purchases_counter;
    }

    /**
     * @param int $purchases_counter
     */
    public function setPurchasesCounter(int $purchases_counter)
    {
        $this->purchases_counter = $purchases_counter;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }

}