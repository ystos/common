<?php

namespace Ystos\Common\Entity\Professional\Payment;

require_once COMMONPATH . '/models/Entity/User/User.php';
require_once COMMONPATH . '/models/Entity/Professional/Service.php';

/**
 * Transaction Model
 *
 * @Entity
 * @Table(name="pro_transactions")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Transaction
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Professional\Service")
     * @JoinColumn(name="service_id", referencedColumnName="id")
     */
    protected $service;
    /**
     * @Column(type="integer", nullable=true)
     */
    protected $value;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $date;
    /**
     * The response code of Mangopay
     * @Column(type="integer", nullable=true)
     */
    protected $response_code;
    /**
     * The identifier of the transaction on Mangopay.
     * @Column(type="integer", nullable=false)
     */
    protected $mangopay_id;
    /**
     *
     * @Column(type="string", nullable=false)
     */
    protected $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getResponseCode()
    {
        return $this->response_code;
    }

    /**
     * @param mixed $response_code
     */
    public function setResponseCode($response_code)
    {
        $this->response_code = $response_code;
    }

    /**
     * @return mixed
     */
    public function getMangopayId()
    {
        return $this->mangopay_id;
    }

    /**
     * @param mixed $mangopay_id
     */
    public function setMangopayId($mangopay_id)
    {
        $this->mangopay_id = $mangopay_id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }


}