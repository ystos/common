<?php

namespace Ystos\Common\Entity\Professional\Payment;
/**
 * Sale Line Model
 *
 * It represent the a line in tyhe bill for example when you sale a service, the costumer whan to add something else, you add it in the bill and it's a sale Line
 *
 *
 * @Entity
 * @Table(name="pro_sale_lines")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Sale_Line
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Professional\Payment\Sale")
     * @JoinColumn(name="sale_id", referencedColumnName="id")
     */
    protected $sale;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Professional\Service")
     * @JoinColumn(name="service_id", referencedColumnName="id", nullable=true)
     */
    protected $service;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $other;
    /**
     * Duty free value
     *
     * @Column(type="float", nullable=true)
     */
    protected $value_dt;
    /**
     * Tax
     *
     * From 0 to 1
     *
     * @Column(type="float", nullable=true)
     */
    protected $applicable_tax;
    /**
     *
     * @Column(type="integer", nullable=true)
     */
    protected $quantity;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @param mixed $sale
     */
    public function setSale($sale)
    {
        $this->sale = $sale;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param mixed $service
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * @param mixed $other
     */
    public function setOther($other)
    {
        $this->other = $other;
    }

    /**
     * @return mixed
     */
    public function getValueDutyFree()
    {
        return $this->value_dt;
    }

    /**
     * @param mixed $value_dt
     */
    public function setValueDutyFree($value_dt)
    {
        $this->value_dt = $value_dt;
    }

    /**
     * @return mixed
     */
    public function getApplicableTax()
    {
        return $this->applicable_tax;
    }

    /**
     * @param mixed $applicable_tax
     */
    public function setApplicableTax($applicable_tax)
    {
        $this->applicable_tax = $applicable_tax;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }


}