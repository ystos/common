<?php

namespace Ystos\Common\Entity\Professional\Payment;

use Doctrine\Common\Collections\ArrayCollection;

require_once COMMONPATH . '/models/Entity/User/User.php';
require_once COMMONPATH . '/models/Entity/Professional/Service.php';
require_once COMMONPATH . '/models/Entity/Professional/Shop.php';
require_once COMMONPATH . '/models/Entity/Professional/Payment/Sale_Line.php';

/**
 * Transaction Model
 *
 * @Entity
 * @Table(name="pro_sales")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Sale
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Professional\Shop")
     * @JoinColumn(name="shop_id", referencedColumnName="id", nullable=false)
     */
    protected $shop;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Professional\Appointment")
     * @JoinColumn(name="appointment_id", referencedColumnName="id", nullable=true)
     */
    protected $appointment;
    /**
     * One Sale has 1 to many sale line
     * @OneToMany(targetEntity="Sale_Line", mappedBy="sale")
     */
    protected $sale_lines;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $bill_number;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $date;
    /**
     * @Column(type="float", nullable=false)
     */
    protected $total_it;
    /**
     * @Column(type="float", nullable=false)
     */
    protected $total_df;
    /**
     * @var boolean
     * @Column(type="boolean", nullable=false)
     */
    protected $refunded = false;

    public function __construct()
    {
        $this->sale_lines = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getAppointment()
    {
        return $this->appointment;
    }

    /**
     * @param mixed $appointment
     */
    public function setAppointment($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * @return mixed
     */
    public function getSaleLines()
    {
        return $this->sale_lines;
    }

    /**
     * @param mixed $sale_lines
     */
    public function setSaleLines($sale_lines)
    {
        $this->sale_lines = $sale_lines;
    }

    public function addSaleLine(Sale_Line $sale_line){
        $this->sale_lines->add($sale_line);
    }

    public function removeSaleLine(Sale_Line $sale_line){
        $this->sale_lines->removeElement($sale_line);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTotalIncludingTax()
    {
        return $this->total_it;
    }

    /**
     * @param mixed $total_it
     */
    public function setTotalIncludingTax($total_it)
    {
        $this->total_it = $total_it;
    }

    /**
     * @return mixed
     */
    public function getTotalDutyFree()
    {
        return $this->total_df;
    }

    /**
     * @param mixed $total_df
     */
    public function setTotalDutyFree($total_df)
    {
        $this->total_df = $total_df;
    }

    /**
     * @return mixed
     */
    public function getBillNumber()
    {
        return $this->bill_number;
    }

    /**
     * @param mixed $bill_number
     */
    public function setBillNumber($bill_number)
    {
        $this->bill_number = $bill_number;
    }

    /**
     * @return boolean
     */
    public function isRefunded()
    {
        return $this->refunded;
    }

    /**
     * @param boolean $refunded
     */
    public function setRefunded($refunded)
    {
        $this->refunded = $refunded;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }


}