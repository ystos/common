<?php

namespace Ystos\Common\Entity\Professional;

require_once COMMONPATH . 'models/Entity/User/User.php';

/**
 * Legal Information Model
 *
 * @Entity
 * @Table(name="pro_legal_informations")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Legal_Information
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $headquarter_address_line_1;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $headquarter_address_line_2;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $headquarter_city;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $headquarter_region;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $headquarter_postalcode;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $headquarter_country;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $representative_firstname;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $representative_lastname;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $representative_birthday;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $representative_country_of_residence;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $representative_nationality;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $representative_email;
    /**
     * User Id provided by mangopay
     *
     * @Column(type="string", length=64,nullable=true)
     */
    protected $m_user_id;
    /**
     * Wallet Id provided by mangopay
     *
     * @Column(type="string", length=64,nullable=true)
     */
    protected $m_seller_wallet_id;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getHeadquarterAddressLine1()
    {
        return $this->headquarter_address_line_1;
    }

    /**
     * @param mixed $headquarter_address_line_1
     */
    public function setHeadquarterAddressLine1($headquarter_address_line_1)
    {
        $this->headquarter_address_line_1 = $headquarter_address_line_1;
    }

    /**
     * @return mixed
     */
    public function getHeadquarterAddressLine2()
    {
        return $this->headquarter_address_line_2;
    }

    /**
     * @param mixed $headquarter_address_line_2
     */
    public function setHeadquarterAddressLine2($headquarter_address_line_2)
    {
        $this->headquarter_address_line_2 = $headquarter_address_line_2;
    }

    /**
     * @return mixed
     */
    public function getHeadquarterCity()
    {
        return $this->headquarter_city;
    }

    /**
     * @param mixed $headquarter_city
     */
    public function setHeadquarterCity($headquarter_city)
    {
        $this->headquarter_city = $headquarter_city;
    }

    /**
     * @return mixed
     */
    public function getHeadquarterRegion()
    {
        return $this->headquarter_region;
    }

    /**
     * @param mixed $headquarter_region
     */
    public function setHeadquarterRegion($headquarter_region)
    {
        $this->headquarter_region = $headquarter_region;
    }

    /**
     * @return mixed
     */
    public function getHeadquarterPostalcode()
    {
        return $this->headquarter_postalcode;
    }

    /**
     * @param mixed $headquarter_postalcode
     */
    public function setHeadquarterPostalcode($headquarter_postalcode)
    {
        $this->headquarter_postalcode = $headquarter_postalcode;
    }

    /**
     * @return mixed
     */
    public function getHeadquarterCountry()
    {
        return $this->headquarter_country;
    }

    /**
     * @param mixed $headquarter_country
     */
    public function setHeadquarterCountry($headquarter_country)
    {
        $this->headquarter_country = $headquarter_country;
    }

    /**
     * @return mixed
     */
    public function getRepresentativeFirstname()
    {
        return $this->representative_firstname;
    }

    /**
     * @param mixed $representative_firstname
     */
    public function setRepresentativeFirstname($representative_firstname)
    {
        $this->representative_firstname = $representative_firstname;
    }

    /**
     * @return mixed
     */
    public function getRepresentativeLastname()
    {
        return $this->representative_lastname;
    }

    /**
     * @param mixed $representative_lastname
     */
    public function setRepresentativeLastname($representative_lastname)
    {
        $this->representative_lastname = $representative_lastname;
    }

    /**
     * @return \DateTime | null
     */
    public function getRepresentativeBirthday()
    {
        return $this->representative_birthday;
    }

    /**
     * @param mixed $representative_birthday
     */
    public function setRepresentativeBirthday($representative_birthday)
    {
        $this->representative_birthday = $representative_birthday;
    }

    /**
     * @return mixed
     */
    public function getRepresentativeCountryOfResidence()
    {
        return $this->representative_country_of_residence;
    }

    /**
     * @param mixed $representative_country_of_residence
     */
    public function setRepresentativeCountryOfResidence($representative_country_of_residence)
    {
        $this->representative_country_of_residence = $representative_country_of_residence;
    }

    /**
     * @return mixed
     */
    public function getRepresentativeNationality()
    {
        return $this->representative_nationality;
    }

    /**
     * @param mixed $representative_nationality
     */
    public function setRepresentativeNationality($representative_nationality)
    {
        $this->representative_nationality = $representative_nationality;
    }

    /**
     * @return mixed
     */
    public function getRepresentativeEmail()
    {
        return $this->representative_email;
    }

    /**
     * @param mixed $representative_email
     */
    public function setRepresentativeEmail($representative_email)
    {
        $this->representative_email = $representative_email;
    }

    /**
     * @return mixed
     */
    public function getMangopayUserId()
    {
        return $this->m_user_id;
    }

    /**
     * @param mixed $m_user_id
     */
    public function setMangopayUserId($m_user_id)
    {
        $this->m_user_id = $m_user_id;
    }

    /**
     * @return mixed
     */
    public function getMangopaySellerWalletId()
    {
        return $this->m_seller_wallet_id;
    }

    /**
     * @param mixed $m_seller_wallet_id
     */
    public function setMangopaySellerWalletId($m_seller_wallet_id)
    {
        $this->m_seller_wallet_id = $m_seller_wallet_id;
    }





    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'product'=> $this->product,
            'date' => $this->date,
        );
    }


}