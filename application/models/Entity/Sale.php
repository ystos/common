<?php

namespace Ystos\Common\Entity;

require_once COMMONPATH . 'models/Entity/User/User.php';
require_once COMMONPATH . 'models/Entity/Product/Archived_Product.php';
require_once COMMONPATH . 'models/Entity/Feedback/User_Feedback.php';
require_once COMMONPATH . 'models/Repository/SaleRepository.php';

use JsonSerializable;
use Ystos\Common\Entity\Feedback\User_Feedback;
use Ystos\Common\Entity\Product\Archived_Product;
use Ystos\Common\Entity\Product\Product;

/**
 * Sale Model
 *
 * @Entity(repositoryClass="Ystos\Common\Repository\SaleRepository")
 * @Table(name="sales")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Sale implements JsonSerializable
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="seller_id", referencedColumnName="id", nullable=false)
     */
    protected $seller;
    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="buyer_id", referencedColumnName="id", nullable=false)
     */
    protected $buyer;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Product\Product")
     * @JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    protected $product;
    /**
     * The price payed by the buyer, because he can negotiate it
     * @Column(type="float", nullable=false)
     */
    protected $price_payed;
    /**
     * The fees taken
     * @Column(type="float", nullable=false)
     */
    protected $fees;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $date;
    /**
     * @OneToOne(targetEntity="Ystos\Common\Entity\Feedback\User_Feedback", mappedBy="sale")
     * @JoinColumn(name="user_feedback_id", referencedColumnName="id", nullable=true)
     */
    protected $user_feedback;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param mixed $seller
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
    }

    /**
     * @return User
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param User $buyer
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getPricePayed()
    {
        return $this->price_payed;
    }

    /**
     * @param mixed $price_payed
     */
    public function setPricePayed($price_payed)
    {
        $this->price_payed = $price_payed;
    }

    /**
     * @return mixed
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * @param mixed $fees
     */
    public function setFees($fees)
    {
        $this->fees = $fees;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return User_Feedback
     */
    public function getUserFeedback()
    {
        return $this->user_feedback;
    }

    /**
     * @param User_Feedback $user_feedback
     */
    public function setUserFeedback($user_feedback)
    {
        $this->user_feedback = $user_feedback;
    }


    public function jsonSerialize()
    {
        /**
         * @var $date \DateTime
         */
        $date = $this->date;
        // TODO
        return array(
            'id' => $this->id,
            'buyer' => $this->buyer,
            'seller' => $this->seller,
            'date' => $date->format('d/m/Y H:m'),
            'fees' => $this->fees,
            'price_payed' => $this->price_payed
        );
    }


}