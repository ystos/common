<?php

namespace Ystos\Common\Entity\User;

require_once COMMONPATH . 'models/Entity/User/User.php';
require_once COMMONPATH . 'models/Entity/Product/Product.php';

/**
 * Subscriptions Model
 *
 * @Entity
 * @Table(name="subscriptions")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Subscription
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $mail_messages;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $mail_favorites;


    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'product'=> $this->product,
            'date' => $this->date,
        );
    }


}