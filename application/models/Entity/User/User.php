<?php

namespace Ystos\Common\Entity;

require_once COMMONPATH . 'models/Repository/User_Repository.php';
require_once COMMONPATH . 'models/Entity/Professional/Shop.php';
require_once COMMONPATH . 'models/Entity/Professional/Appointment.php';

use Doctrine\Common\Collections\ArrayCollection;
use Ystos\Common\Entity\Professional\Shop;
use Ystos\Common\Entity\User\Facebook_User_account;
use Ystos\Common\Entity\User\Google_User_account;
use Ystos\Common\Entity\User\User_account;
use phpDocumentor\Reflection\Types\Integer;


/**
 * User Model
 *
 * @Entity(repositoryClass="Ystos\Common\Repository\User_Repository")
 * @Table(name="users_data")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class User
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $first_name;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $last_name;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $city;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $email;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $phone;
    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $status;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $registration_date;
    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $last_login;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $login_ip;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $picture;
    /**
     * @Column(type="text", nullable=true)
     */
    protected $biography;
    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $birthday;
    /**
     * @var $account Facebook_User_account | Google_User_account | User_account
     */
    protected $account;
    /**
     * Many Users can have Many Shops
     * @ManyToMany(targetEntity="Ystos\Common\Entity\Professional\Shop", inversedBy="users")
     * @JoinTable(name="pro_users_shops")
     */
    protected $shops;
    /**
     * Many Users can have Many appointment
     * @OneToMany(targetEntity="Ystos\Common\Entity\Professional\Appointment", mappedBy="user")
     */
    protected $appointments;
    /**
     * Stored in Redis
     * Sells counter
     * @var $sells_counter Integer
     */
    protected $sells_counter;
    /**
     * Stored in Redis
     * Purchases counter
     * @var $purchases_counter Integer
     */
    protected $purchases_counter;

    public function __construct()
    {
        $this->shops = new ArrayCollection();
        $this->appointment = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $first_name
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $last_name
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /*    /**
 * @return mixed
 */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getRegistrationDate()
    {
        return $this->registration_date;
    }

    /**
     * @param mixed $registration_date
     */
    public function setRegistrationDate($registration_date)
    {
        $this->registration_date = $registration_date;
    }

    /**
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * @param mixed $last_login
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
    }

    /**
     * @return mixed
     */
    public function getLoginIp()
    {
        return $this->login_ip;
    }

    /**
     * @param mixed $login_ip
     */
    public function setLoginIp($login_ip)
    {
        $this->login_ip = $login_ip;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * @param mixed $biography
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return Facebook_User_account|Google_User_account|User_account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Facebook_User_account|Google_User_account|User_account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return mixed
     */
    public function getShops()
    {
        return $this->shops;
    }


    public function add_shop(Shop $shop = null)
    {
        $this->shops->add($shop);
    }

    public function remove_shop(Shop $shop)
    {
        $this->shops->removeElement($shop) ;
    }

    /**
     * @return int
     */
    public function getSellsCounter()
    {
        return $this->sells_counter;
    }

    /**
     * @param int $sells_counter
     */
    public function setSellsCounter($sells_counter)
    {
        $this->sells_counter = $sells_counter;
    }

    /**
     * @return int
     */
    public function getPurchasesCounter()
    {
        return $this->purchases_counter;
    }

    /**
     * @param int $purchases_counter
     */
    public function setPurchasesCounter($purchases_counter)
    {
        $this->purchases_counter = $purchases_counter;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'last_login' => $this->last_login,
            'login_ip' => $this->login_ip,
            'status' => $this->status,
            'registration_date' => $this->registration_date,
            'account' => $this->account,
            'provider' => $this->provider,
            'picture' => $this->picture,
            'biography' => $this->biography
        );
    }

    public function to_array(){
        return array(
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'last_login' => $this->last_login,
            'login_ip' => $this->login_ip,
            'status' => $this->status,
            'registration_date' => $this->registration_date,
            'account' => $this->account,
            'provider' => $this->provider,
            'picture' => $this->picture,
            'biography' => $this->biography
        );
    }


}