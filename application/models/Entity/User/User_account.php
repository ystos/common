<?php

namespace Ystos\Common\Entity\User;

require_once COMMONPATH . 'models/Entity/User/User.php';

/**
 * User Account Model
 *
 * @Entity
 * @Table(name="users_account")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class User_account
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @OneToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $password;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $salt;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $token;
    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $password_update_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getPasswordUpdateDate()
    {
        return $this->password_update_date;
    }

    /**
     * @param mixed $password_update_date
     */
    public function setPasswordUpdateDate($password_update_date)
    {
        $this->password_update_date = $password_update_date;
    }


    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'product'=> $this->product,
            'date' => $this->date,
        );
    }


}