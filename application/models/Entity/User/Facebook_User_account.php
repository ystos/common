<?php

namespace Ystos\Common\Entity\User;


require_once COMMONPATH . 'models/Entity/User/User.php';

/**
 * Facebook User Account Model
 *
 * @Entity
 * @Table(name="facebook_users_account")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Facebook_User_account
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @OneToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $provider_id;
    /**
     * @Column(type="string", nullable=false)
     */
    protected $token;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getProviderId()
    {
        return $this->provider_id;
    }

    /**
     * @param mixed $provider_id
     */
    public function setProviderId($provider_id)
    {
        $this->provider_id = $provider_id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }


    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'product'=> $this->product,
            'date' => $this->date,
        );
    }


}