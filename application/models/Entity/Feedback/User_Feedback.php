<?php

namespace Ystos\Common\Entity\Feedback;

require_once COMMONPATH.'models/Entity/Sale.php';

use Ystos\Common\Entity\Sale;


/**
 * User Feedback Model
 * It used to store the user feedbacks entered after to confirm the sale
 * @Entity
 * @Table(name="users_feedbacks")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class User_Feedback
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * The sale that this feed back concern
     * @OneToOne(targetEntity="Ystos\Common\Entity\Sale", inversedBy="user_feedback")
     * @JoinColumn(name="sale_id", referencedColumnName="id", nullable=false)
     */
    protected $sale;
    /**
 * It store the questions and the marks ( on 5 ) in the array
 * array =>
 *  id_question => mark
 * @Column(type="array", nullable=true)
 */
    protected $marks;
    /**
     * It's the average mark
     * @Column(type="float", nullable=true)
     */
    protected $average_mark;
    /**
     * @Column(type="text", nullable=true)
     */
    protected $comment;
    /**
     * We can disable a feedback.
     * @Column(type="boolean", nullable=false)
     */
    protected $active;
    /**
     * Token used to identify the user feedback. We used it in the mail send to the user.
     * @Column(type="text", nullable=true)
     */
    protected $token;
    /**
     * The date we send the email, it use to unvalidate the token after a period #security
     * @Column(type="datetime", nullable=false)
     */
    protected $date_sent;
    /**
     * The date the user complete the feedback form
     * @Column(type="datetime", nullable=true)
     */
    protected $date_complete;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Sale
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @param Sale $sale
     */
    public function setSale($sale)
    {
        $this->sale = $sale;
    }

    /**
     * @return array | null
     */
    public function getMarks()
    {
        return $this->marks;
    }

    /**
     * @param array | null $marks
     */
    public function setMarks($marks)
    {
        $this->marks = $marks;
    }

    /**
     * @return float
     */
    public function getAverageMark()
    {
        return $this->average_mark;
    }

    /**
     * @param float $average_mark
     */
    public function setAverageMark($average_mark)
    {
        $this->average_mark = $average_mark;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getDateSent()
    {
        return $this->date_sent;
    }

    /**
     * @param mixed $date_sent
     */
    public function setDateSent($date_sent)
    {
        $this->date_sent = $date_sent;
    }

    /**
     * @return mixed
     */
    public function getDateComplete()
    {
        return $this->date_complete;
    }

    /**
     * @param mixed $date_complete
     */
    public function setDateComplete($date_complete)
    {
        $this->date_complete = $date_complete;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'sale' => $this->sale,
            'comment' => $this->comment,
            'marks' => $this->marks,
            'average_mark' => $this->average_mark,
            'active'=> $this->active,
        );
    }


}