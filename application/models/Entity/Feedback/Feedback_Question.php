<?php

namespace Ystos\Common\Entity\Feedback;

/**
 * Feedback Question Model
 * It used to store the feedback question
 * @Entity
 * @Table(name="users_feedback_questions")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Feedback_Question
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @Column(type="string", length=200, nullable=false)
     */
    protected $question_label;
    /**
     * @Column(type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getQuestionLabel()
    {
        return $this->question_label;
    }

    /**
     * @param mixed $question_label
     */
    public function setQuestionLabel($question_label)
    {
        $this->question_label = $question_label;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }



    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'label' => $this->question_label,
            'active'=> $this->active,
        );
    }


}