<?php

namespace Ystos\Common\Entity\Brand_Manager;


require_once COMMONPATH . '/models/Entity/User/User.php';
require_once COMMONPATH . '/models/Entity/Brand_Manager/Brand_Manager.php';
require_once COMMONPATH . '/models/Entity/Product/Product.php';

/**
 * Sponsorship sale model
 *
 * @Entity
 * @Table(name="bm_sponsorship_sale")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Sponsorship_Sale
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Brand_Manager\Brand_Manager")
     * @JoinColumn(name="bm_id", referencedColumnName="id", nullable=false)
     */
    protected $brand_manager;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Product\Product")
     * @JoinColumn(name="classified_id", referencedColumnName="id", nullable=false)
     */
    protected $classified;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $price_payed;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $commission;
    /**
     * @Column(type="float", nullable=true)
     */
    protected $bm_commission;
    /**
     * @var \DateTime
     * @Column(type="datetime", nullable=false)
     */
    protected $date;
    /**
     * @var boolean
     * @Column(type="boolean", nullable=false)
     */
    protected $is_payed = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBrandManager()
    {
        return $this->brand_manager;
    }

    /**
     * @param mixed $brand_manager
     */
    public function setBrandManager($brand_manager)
    {
        $this->brand_manager = $brand_manager;
    }

    /**
     * @return mixed
     */
    public function getClassified()
    {
        return $this->classified;
    }

    /**
     * @param mixed $classified
     */
    public function setClassified($classified)
    {
        $this->classified = $classified;
    }

    /**
     * @return mixed
     */
    public function getPricePayed()
    {
        return $this->price_payed;
    }

    /**
     * @param mixed $price_payed
     */
    public function setPricePayed($price_payed)
    {
        $this->price_payed = $price_payed;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $commission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getBrandManagerCommission()
    {
        return $this->bm_commission;
    }

    /**
     * @param mixed $bm_commission
     */
    public function setBrandManagerCommission($bm_commission)
    {
        $this->bm_commission = $bm_commission;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return bool
     */
    public function isPayed()
    {
        return $this->is_payed;
    }

    /**
     * @param bool $is_payed
     */
    public function setIsPayed(bool $is_payed)
    {
        $this->is_payed = $is_payed;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
        );
    }

}