<?php

namespace Ystos\Common\Entity\Brand_Manager;

require_once COMMONPATH . 'models/Entity/User/User.php';
require_once COMMONPATH . 'models/Entity/Brand_Manager/Brand_Manager.php';

/**
 * Sponsorship model
 *
 * @Entity
 * @Table(name="bm_sponsorship")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Sponsorship
{
    const STATUS_ASSOCIATED = 1;
    const STATUS_PENDING = 2;
    const STATUS_REJECTED = 3;

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Brand_Manager\Brand_Manager")
     * @JoinColumn(name="bm_id", referencedColumnName="id", nullable=false)
     */
    protected $brand_manager;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $firstname;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $lastname;
    /**
     * @Column(type="string", nullable=true)
     */
    protected $email;
    /**
     * 1 -> Associated
     * 2 -> Pending
     * 3 -> Rejected
     *
     * @Column(type="integer", nullable=false)
     */
    protected $status;
    /**
     * @OneToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="seller_id", referencedColumnName="id", nullable=true)
     *
     */
    protected $seller;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBrandManager()
    {
        return $this->brand_manager;
    }

    /**
     * @param mixed $brand_manager
     */
    public function setBrandManager($brand_manager)
    {
        $this->brand_manager = $brand_manager;
    }


    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param mixed $seller
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
    }

    /**
     * This function prepare an array which will be used to be returned in a json,
     * Please do not include confidential, and critical information, like the password.
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'email' => $this->email
        );
    }

    public function to_array()
    {
        return [
            'id' => $this->id,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'email' => $this->email
        ];
    }


}