<?php

namespace Ystos\Common\Entity\Messaging;

use Ystos\Common\Entity\User;

require_once COMMONPATH . 'models/Entity/User/User.php';
require_once COMMONPATH . 'models/Entity/Product/Product.php';
require_once COMMONPATH.'models/Repository/MessageRepository.php';

/**
 * Message Model
 *
 * @Entity(repositoryClass="Ystos\Common\Repository\MessageRepository")
 * @Table(name="messages")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Message
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="sender_id", referencedColumnName="id", nullable=false)
     */
    protected $sender;
    /**
     * Many messages are in one conversation
     * @ManyToOne(targetEntity="Conversation", inversedBy="messages")
     * @JoinColumn(name="conversation_id", referencedColumnName="id",onDelete="CASCADE")
     */
    protected $conversation;
    /**
     * @Column(type="text", nullable=false)
     */
    protected $content;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $send_date;
    /**
     * A flag that tell if the receiver has seen the message
     * @Column(type="boolean", nullable=false)
     */
    protected $open;
    /**
     * A flag that tell if the message is disable or not, it used in case it is possibly a spam
     * @Column(type="boolean", nullable=false)
     */
    protected $disable;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param User $user
     */
    public function setSender(User $user)
    {
        $this->sender = $user;
    }

    /**
     * @return Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * @param Conversation $conversation
     */
    public function setConversation($conversation)
    {
        $this->conversation = $conversation;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return strip_tags($this->content);
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = strip_tags($content);
    }

    /**
     * @return mixed
     */
    public function getSendDate()
    {
        return $this->send_date;
    }

    /**
     * @param mixed $send_date
     */
    public function setSendDate($send_date)
    {
        $this->send_date = $send_date;
    }


    /**
     * @return boolean
     */
    public function isOpen()
    {
        return $this->open;
    }

    /**
     * @param boolean $open
     */
    public function setOpen($open)
    {
        $this->open = $open;
    }

    /**
     * @return boolean
     */
    public function isDisabled()
    {
        return $this->open;
    }

    /**
     * @param boolean $disabled
     */
    public function setDisabled($disabled)
    {
        $this->disable = $disabled;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'sender' => $this->sender,
            'content'=> $this->content,
            'date' => $this->send_date,
            'open' => $this->open,
        );
    }


}