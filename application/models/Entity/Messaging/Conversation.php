<?php

namespace Ystos\Common\Entity\Messaging;

use Doctrine\Common\Collections\ArrayCollection;
use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\User;

require_once COMMONPATH . 'models/Entity/User/User.php';
require_once COMMONPATH  . 'models/Entity/Product/Product.php';
require_once COMMONPATH .'models/Entity/Messaging/Message.php';
require_once COMMONPATH .'models/Repository/MessageRepository.php';

/**
 * Conversation Model
 *
 * @Entity
 * @Table(name="conversation")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Conversation
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * The user interested by the product
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * The user that sell the product
     * @ManyToOne(targetEntity="Ystos\Common\Entity\User")
     * @JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     */
    protected $owner;
    /**
     * The product concerned
     * @ManyToOne(targetEntity="Ystos\Common\Entity\Product\Product")
     * @JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $product;
    /**
     * One conversation can have many messages
     *
     * @OneToMany(targetEntity="Message",mappedBy="conversation")
     */
    protected $messages;
    /**
     * @Column(type="datetime", nullable=false)
     */
    protected $start_date;
    /**
     * Update when there is a new message
     * @Column(type="datetime", nullable=true)
     */
    protected $update_date;
    /**
     * A flag that tell if the message is disable or not, it used in case it is possibly a spam
     * @Column(type="boolean", nullable=false)
     */
    protected $disable;
    /**
     * Token used to identify the conversation. We used it in the mail send to the user.
     * @Column(type="text", nullable=true)
     */
    protected $token;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param User $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param mixed $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @param Message|null $message
     */
    public function addMessage(Message $message = null)
    {
        $this->messages->add($message);
    }

    /**
     * @param Message $message
     */
    public function removeMessage(Message $message)
    {
        $this->messages->removeElement($message) ;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param \DateTime $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->update_date;
    }

    /**
     * @param \DateTime $update_date
     */
    public function setUpdateDate($update_date)
    {
        $this->update_date = $update_date;
    }

    /**
     * @return boolean
     */
    public function isDisable()
    {
        return $this->disable;
    }

    /**
     * @param boolean $disable
     */
    public function setDisable($disable)
    {
        $this->disable = $disable;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'user' => $this->user,
            'owner'=> $this->owner,
            'product' => $this->product,
            'messages'=> $this->messages->toArray(),
            'token' => $this->token,
            'start_date' => $this->start_date,
        );
    }


}