<?php

namespace Ystos\Common\Entity\Messaging;

require_once COMMONPATH . 'models/Entity/Messaging/Message.php';

/**
 * Filtered Message Model
 * It used to store the message that are possibly a spam
 * @Entity(repositoryClass="Ystos\Common\Repository\Messaging\Filtered_Message_Repository")
 * @Table(name="filtered_messages")
 * @author  Etienne Pichereau <pichereau.e@gmail.com>
 */
class Filtered_Message
{
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @OneToOne(targetEntity="Message")
     * @JoinColumn(name="message_id", referencedColumnName="id", nullable=false)
     */
    protected $message;
    /**
     * @Column(type="float", nullable=false)
     */
    protected $probability;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param Message $message
     */
    public function setMessage(Message $message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getProbability()
    {
        return $this->probability;
    }

    /**
     * @param mixed $probability
     */
    public function setProbability($probability)
    {
        $this->probability = $probability;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'message' => $this->message,
            'probability'=> $this->probability,
        );
    }


}