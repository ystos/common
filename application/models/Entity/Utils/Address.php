<?php

namespace Ystos\Common\Entity\Utils;


/**
 * Address
 *
 * @Table(name="u_address")
 * @Entity
 */
class Address
{
    /**
     * @var int
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @Column( type="string", nullable=true)
     */
    private $address_long;
    /**
     * @var string
     *
     * @Column( type="string", nullable=true)
     */
    private $address;
    /**
     * @var string
     *
     * @Column( type="string", nullable=true)
     */
    private $address_line_2;
    /**
     * @var string
     *
     * @Column( type="string", nullable=true)
     */
    private $city;
    /**
     * @var string
     *
     * @Column( type="string", nullable=true)
     */
    private $postal_code;
    /**
     * @var string
     *
     * @Column( type="string", nullable=true)
     */
    private $country;
    /**
     * @var string
     *
     * @Column(type="decimal", precision=9, scale=6, nullable=true)
     */
    private $latitude;
    /**
     * @var string
     *
     * @Column(type="decimal", precision=9, scale=6, nullable=true)
     */
    private $longitude;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddressLong()
    {
        return $this->address_long;
    }

    /**
     * @param string $address_long
     */
    public function setAddressLong($address_long)
    {
        $this->address_long = $address_long;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->address_line_2;
    }

    /**
     * @param string $address_line_2
     */
    public function setAddressLine2($address_line_2)
    {
        $this->address_line_2 = $address_line_2;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param string $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

}
