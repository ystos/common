<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$environment = getenv('ENVIRONMENT');

/**
 * ------------------------------------------------------------------------
 *
 * Application related config
 *
 * ------------------------------------------------------------------------
 */
if($environment==='development'){
    $config['base_api_url'] = 'https://dev.api.ystos.com/';

    $config['base_public_url'] = 'https://dev.ystos.com/';
}
else if($environment==='local'){
    $config['base_api_url'] = 'http://api.ystos.local:81/';
    $config['base_public_url'] = 'http://ystos.local:81/';
}
else{
    $config['base_api_url'] = 'https://api.ystos.com/';
    $config['base_public_url'] = 'https://www.ystos.com/';
}


// Sendinblue API key
$config['sendinblue_api_key'] = 'O3McnGmr87IS9dag';

// Function that defined the commission taken during the sale
$config['commission_function'] = function ($price){
    $commission = 0.07;
    return $commission;
};

$config['mailjet_secret_key'] = 'ece388e98a8425b6c3b57eabf2a4baf9';
$config['mailjet_public_key'] = '08b1369c19b2e1617b97a3e9e36a4ce5';
$config['mailjet_sender_address'] = 'hello@ystos.com';

$config['mailtrap_config'] = [
    'protocol' => 'smtp',
    'smtp_host' => 'smtp.mailtrap.io',
    'smtp_port' => 2525,
    'smtp_user' => 'e23d0a31678642',
    'smtp_pass' => '4d4bde1d44038c',
    'crlf' => "\r\n",
    'newline' => "\r\n"
];

$config['company_name'] = 'Ystos';

$config['social'] = array(
    'facebook' => array('url' => 'https://www.facebook.com/Ystos'),
    'twitter' => array('url' => 'https://twitter.com/Ecommerce_Ystos'),
    'blog' => array('url' => 'http://ystos.com/blog')
);

$config['contact_address'] = 'hello@ystos.com';

$config['mail_logo'] = 'https://www.ystos.com/assets/core/medias/logo_full.svg';


/** ---------------------------
 * MANGOPAY
 */
if(getenv('ENVIRONMENT') === 'development' || getenv('ENVIRONMENT') === 'local'){
    /**
     * The CLIENTID porvided by Mangopay
     */
    $config['mangopay_client_id']= 'pohhiifehhdajohecf';
    /**
     * The password of the account
     */
    $config['mangopay_password']= 'BdwHdR2aZ6GOgbO9be1jDtLi9nmXPOfmCad6Fd77vp2Vuqr3f4';
    /**
     * Url call for use the mangopay api
     *
     * PRODUCTION => https://api.mangopay.com
     * TEST => https://api.sandbox.mangopay.com
     */
    $config['mangopay_base_url']= 'https://api.sandbox.mangopay.com';
    /**
     * Enable or disable debug mode, it provide more information on the request send to mangopay
     * Set to false in production
     */
    $config['mangopay_debug']= FALSE;
}
else{
    $config['mangopay_client_id']= 'ystoscmagique';
    $config['mangopay_password']= 'Wb6vfjcznPPZngPOvwCGYXTt3YLibY21iQueZm4jpziKuVyB0X';
    $config['mangopay_base_url']= 'https://api.mangopay.com';
    $config['mangopay_debug']= FALSE;
}
/**
 * Folder where the temporary files will be stored
 */
$config['mangopay_tmp_folder']= FCPATH.'/temp/mangopay';
/**
 * Callback URLS
 */
$config['mangopay_payment_callback_url']= $config['base_public_url'] .'purchase/end';
/**
 * Bank reference, the name that appear on the bank account
 */
$config['mangopay_bank_ref'] = 'Ystos.com';
/**
 * Choose the default currency
 */
$config['mangopay_default_currency'] = 'EUR';

/** ------------------
 * Redis
 */
if($environment === 'development'){
    $config['redis_url'] = 'localhost:6379';
    $config['redis_database'] = '2';
}
elseif($environment === 'local'){
    $config['redis_url'] = '192.168.99.100:32772';
    $config['redis_database'] = '1';
}
else{
    $config['redis_url'] = 'localhost:6379';
    $config['redis_database'] = '1';
}

if($environment === 'development'){
    $config['public_assets_path'] = '/var/www/vhosts/dev.ystos.com/httpdocs/assets/';
    $config['public_assets_url'] = 'https://dev.ystos.com/assets';
}
elseif($environment === 'local'){
    $config['public_assets_path'] = 'C:\Users\piche\Dropbox\Applications\Heroku\ystos-dev-public\assets'.DIRECTORY_SEPARATOR;
    $config['public_assets_url'] = 'http://ystos.local:81/assets/';
}
else{
    $config['public_assets_path'] = '/var/www/vhosts/ystos.com/httpdocs/assets/';
    $config['public_assets_url'] = 'https://ystos.com/assets/';
}