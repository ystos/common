<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 15/08/2017
 * Time: 14:32
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(getenv('ENVIRONMENT') === 'development'){
    $config['es_server'] = 'http://localhost:9200';
    $config['index'] = 'ystos_dev';
}
elseif (getenv('ENVIRONMENT') === 'local'){
    $config['es_server'] = 'http://192.168.99.100:32770';
    $config['index'] = 'ystos';
}
else{
    $config['es_server'] = 'http://localhost:9200';
    $config['index'] = 'ystos';
}