<?php
/**
 * Created by PhpStorm.
 * User: Etienne
 * Date: 01/11/2017
 * Time: 16:45
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('get_service_link'))
{
    function get_service_link(\Ystos\Common\Entity\Professional\Service $service)
    {
        return 'services/'.$service->getShop()->getName().'/'.$service->getId();
    }
}