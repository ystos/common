<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 28/06/2017
 * Time: 17:37
 */

namespace Ystos\Common\Service;

use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\Professional\Service;

class Search_Service
{

    protected $CI;

    public function __construct()
    {
        try {
            $this->CI =& get_instance();

            log_message('debug','Get instance');
            $this->CI->load->library('Elasticsearch');

            log_message('debug', 'Elastic loaded');
            $mapping = array(
                'mappings' => array(
                    "product" => array(
                        "properties" => array(
                            "publish_date" => array(
                                "type" => "date"
                            ),
                            "location" => array(
                                "type" => "geo_point"
                            ),
                            "price" =>
                                ["type" => "float"]
                        )
                    ),
                    "services" => array(
                        "properties" => array(
                            "publish_date" => array(
                                "type" => "date"
                            ),
                            "location" => array(
                                "type" => "geo_point"
                            ),
                            "price" =>
                                ["type" => "float"],
                            "in_home" =>
                                ["type" => "boolean"],
                            "in_shop" =>
                                ["type" => "boolean"]
                        )
                    ),
                )
            );

            $this->CI->elasticsearch->create($mapping);
            log_message('debug', 'Elastic created');
        }
        catch(\Exception $e){
            log_message('error', 'Search service: '.$e->getMessage());
        }
    }

    public function add_product(\Ystos\Common\Entity\Product\Product $product)
    {
        $data = array(
            "title" => $product->getTitle(),
            "description" => $product->getDescription(),
            "price" => $product->getPrice(),
            'publish_date' => $product->getPublishDate()->getTimestamp(),
            "category" => array(
                'name' => $product->getCategory()->getName(),
                'id' => $product->getCategory()->getId()
            ),
            "characteristics" => implode(' , ', $product->getCharacteristics()),
        );

        if ($product->getLongitude() !== null) {
            $data["location"] = array(
                "lat" => $product->getLatitude(),
                "lon" => $product->getLongitude()
            );
        }

        return $this->CI->elasticsearch->add("product", $product->getId(), $data);
    }

    public function add_service(Service $service){
        $data = array(
            "name" => $service->getName(),
            "description" => $service->getDescription(),
            "price" => $service->getPrice(),
            'publish_date' => $service->getPublishDate()->getTimestamp(),
            "in_home" => $service->getInHome(),
            "in_shop" => $service->getInShop(),
            "shop_name" => $service->getShop()->getName(),
            "city" => $service->getShop()->getCity(),
        );

        if($service->getEditDate() !== null){
            $data['edit_date'] = $service->getEditDate()->getTimestamp();
        }

        if ($service->getShop()->getLongitude() !== null) {
            $data["location"] = array(
                "lat" => $service->getShop()->getLatitude(),
                "lon" => $service->getShop()->getLongitude()
            );
        }

        return $this->CI->elasticsearch->add("services", $service->getId(), $data);
    }

    public function search_services_by_coordinate($long, $lat, $radius = 5000, $q = null, $query_params = null)
    {
        $data = array();
        $data['from'] = 0;
        $data['size'] = 500;

        if ($long !== null && $long !== '' && $lat !== null && $lat !== '') {
            $data['query'] = array(
                "bool" => array(
                    "filter" => array(
                        "geo_distance" => array(
                            "distance" => $radius . "m",
                            "distance_type" => "plane",
                            "location" => array(
                                "lat" => $lat,
                                "lon" => $long
                            )
                        )
                    )

                )
            );
        }
        // We add the query if need
        if (isset($q) && $q !== null && !empty($q)) {

            $data['query']['bool']['must'] = array(
                'multi_match' => array(
                    "query" => $q,
                    "fields" => ['name', 'description', 'shop_name']
                ));

        } else {
            $data['query']['bool']['must'] = array("match_all" => (object)array());
        }

        if (isset($query_params['categories']) && $query_params['categories'] !== null) {
            $data['query']['bool']['filter'] = array('terms' => ["category.id" => $query_params['categories']]);
        }

        if ((isset($query_params['min_price']) && $query_params['min_price'] !== null) OR (isset($query_params['max_price']) && $query_params['max_price'] !== null)) {
            $data['query']['bool']['must'] =
                ['range' =>
                    [
                        'price' => []
                    ]
                ];

            if (isset($query_params['min_price']) && $query_params['min_price'] !== null) {
                $data['query']['bool']['must']["range"]['price'] = ['gte' => intval($query_params['min_price'])];
            }

            if (isset($query_params['max_price']) && $query_params['max_price'] !== null) {
                $data['query']['bool']['must']["range"]['price'] = ['lte' => intval($query_params['max_price'])];
            }
        }

        $data['sort'] = [
            "edit_date" => ["order" => "desc"]
        ];

        return $this->CI->elasticsearch->advancedquery('services', $data);
    }

    public function search_last_services($limit)
    {

        $data = array(

            "sort" => array(
                "edit_date" => array("order" => "desc")
            ),
            "size" => $limit,
            'from' => 0
        );

        return $this->CI->elasticsearch->advancedquery('services', $data);

    }


    public function search_products_by_coordinate($long, $lat, $radius = 5000, $q = null, $query_params = null)
    {
        $data = array();
        $data['from'] = 0;
        $data['size'] = 500;

        if ($long !== null && $long !== '' && $lat !== null && $lat !== '') {
            $data['query'] = array(
                "bool" => array(
                    "filter" => array(
                        "geo_distance" => array(
                            "distance" => $radius . "m",
                            "distance_type" => "plane",
                            "location" => array(
                                "lat" => $lat,
                                "lon" => $long
                            )
                        )
                    )

                )
            );
        }
        // We add the query if need
        if (isset($q) && $q !== null && !empty($q)) {

            $data['query']['bool']['must'] = array(
                'multi_match' => array(
                    "query" => $q,
                    "fields" => ['title', 'description', 'characteristics']
                ));

        } else {
            $data['query']['bool']['must'] = array("match_all" => (object)array());
        }

        if (isset($query_params['categories']) && $query_params['categories'] !== null) {
            $data['query']['bool']['filter'] = array('terms' => ["category.id" => $query_params['categories']]);
        }

        if ((isset($query_params['min_price']) && $query_params['min_price'] !== null) OR (isset($query_params['max_price']) && $query_params['max_price'] !== null)) {
            $data['query']['bool']['must'] =
                ['range' =>
                    [
                        'price' => []
                    ]
                ];

            if (isset($query_params['min_price']) && $query_params['min_price'] !== null) {
                $data['query']['bool']['must']["range"]['price'] = ['gte' => intval($query_params['min_price'])];
            }

            if (isset($query_params['max_price']) && $query_params['max_price'] !== null) {
                $data['query']['bool']['must']["range"]['price'] = ['lte' => intval($query_params['max_price'])];
            }
        }

        //echo \GuzzleHttp\json_encode($data);


        $data['sort'] = [
            "publish_date" => ["order" => "desc"]
        ];


        //return ;
        //return $data;
        return $this->CI->elasticsearch->advancedquery('product', $data);
    }

    public function search_product_by_category(array $categories_array)
    {

        $data = array(
            "query" => array(
                "bool" => array(
                    "filter" => array(
                        "terms" => array(
                            "category.id" => $categories_array
                        )
                    )
                )
            ),
            "sort" => array(
                "publish_date" => array("order" => "desc")
            ),
        );

        return $this->CI->elasticsearch->advancedquery('product', $data);

    }

    public function search_product($q)
    {
        $data = array(
            "query" => array(
                "multi_match" => array(
                    "query" => $q,
                    "fields" => ['title', 'description', 'characteristics']
                )
            ),
            "sort" => array(
                "publish_date" => array("order" => "desc")
            ),
        );

        return $this->CI->elasticsearch->advancedquery('product', $data);
    }

    public function search_last_products($limit)
    {

        $data = array(

            "sort" => array(
                "publish_date" => array("order" => "desc")
            ),
            "size" => $limit,
            'from' => 0
        );

        return $this->CI->elasticsearch->advancedquery('product', $data);

    }

    public function remove_product(Product $product)
    {
        return $this->CI->elasticsearch->delete('product', $product->getId());
    }


}