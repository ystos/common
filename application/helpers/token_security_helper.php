<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('setNewRequestToken'))
{
    function setNewRequestToken()
    {
        $CI =& get_instance();
        $request_token = hash('sha256', sha1(uniqid()));
        $CI->session->set_userdata('request_token', $request_token);
    }
}

if (!function_exists('getRequestToken'))
{
    function getRequestToken()
    {
        $CI =& get_instance();
        if ($CI->session->has_userdata('request_token')) {
            return $CI->session->userdata('request_token');
        }
        return "";
    }
}

if (!function_exists('requestTokenVerification'))
{
    function requestTokenVerification($token)
    {
        $CI =& get_instance();
        if ($CI->session->has_userdata('request_token')) {
            return $CI->session->userdata('request_token') == $token;
        }
        return false;
    }
}

if (!function_exists('setNewOfferRequestToken'))
{
    function setNewOfferRequestToken($user_id,$classified_id,$price,$time)
    {
        $CI =& get_instance();
        $offer_request_token = hash('sha256', $user_id.$classified_id.$price.$time);
        $CI->session->set_userdata('offer_request_token', $offer_request_token);
    }
}

/*if (!function_exists('getOfferRequestToken'))
{
    function getOfferRequestToken()
    {
        $CI =& get_instance();
        if ($CI->session->has_userdata('offer_request_token')) {
            return $CI->session->userdata('offer_request_token');
        }
        return "";
    }
}*/

if (!function_exists('offerRequestTokenVerification'))
{
    function offerRequestTokenVerification($user_id,$classified_id,$price,$time)
    {
        $CI =& get_instance();
        if ($CI->session->has_userdata('offer_request_token')) {
            return $CI->session->userdata('offer_request_token') == hash('sha256', $user_id.$classified_id.$price.$time);
        }
        return false;
    }
}