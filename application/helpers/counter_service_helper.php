<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 28/06/2017
 * Time: 17:37
 */
namespace Ystos\Common\Service;


use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\Sale;
use Ystos\Common\Entity\User;

class Counter
{

    protected $CI;

    protected $redis;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->redis = new \Predis\Client([
            'host' => $this->CI->config->item('redis_url'),
            'database' => $this->CI->config->item('redis_database')
        ]);
    }

    public function add_product_view(Product $product, $fingerprint, $connected_user = null){
        if($connected_user === null){
            $this->redis->hincrby("count_view_product:".$product->getId(), $fingerprint, 1);
        }
        else{
            /**
             * @var $connected_user \Ystos\Common\Entity\User
             */
            $this->redis->hincrby("count_view_product:".$product->getId(), 'user:'.$connected_user->getId(), 1);
        }
    }

    public function get_product_views(Product $product){
        return $this->redis->hlen("count_view_product:".$product->getId());
    }

    public function add_registered_user()
    {
        $now = microtime(true);

        $this->redis->hincrby("count_user_minutes", $this->minutes_since_1970($now), 1);
        $this->redis->hincrby("count_user_hours", $this->hours_since_1970($now), 1);
        $this->redis->hincrby("count_user_days", $this->days_since_1970($now), 1);
        $this->redis->incr("count_users");

    }

    public function add_sale_completed()
    {
        $now = microtime(true);

        $this->redis->hincrby("count_sale_completed_minutes", $this->minutes_since_1970($now), 1);
        $this->redis->hincrby("count_sale_completed_hours", $this->hours_since_1970($now), 1);
        $this->redis->hincrby("count_sale_completed_days", $this->days_since_1970($now), 1);
        $this->redis->incr("count_sale_completed");
    }

    public function user_add_sale_completed(User $user)
    {

        $this->redis->hincrby("count_user_sale_completed", $user->getId(), 1);
    }

    public function user_add_purchase_completed(User $user)
    {

        $this->redis->hincrby("count_user_purchase_completed", $user->getId(), 1);
    }

    public function user_get_sale_completed(User $user)
    {
        if ($this->redis->hexists("count_user_sale_completed", $user->getId())) {
            return $this->redis->hget("count_user_sale_completed", $user->getId());
        } else {
            return 0;
        }
    }

    public function user_get_purchase_completed(User $user)
    {
        if ($this->redis->hexists("count_user_purchase_completed", $user->getId())) {
            return $this->redis->hget("count_user_purchase_completed", $user->getId());
        } else {
            return 0;
        }
    }


    public function add_pending_sale()
    {
        $now = microtime(true);

        $this->redis->hincrby("count_sale_pending_minutes", $this->minutes_since_1970($now), 1);
        $this->redis->hincrby("count_sale_pending_hours", $this->hours_since_1970($now), 1);
        $this->redis->hincrby("count_sale_pending_days", $this->days_since_1970($now), 1);
    }

    public function add_fees_taken($fees)
    {
        $now = microtime(true);

        $this->redis->hincrbyfloat("count_fees_minutes", $this->minutes_since_1970($now), $fees);
        $this->redis->hincrbyfloat("count_fees_hours", $this->hours_since_1970($now), $fees);
        $this->redis->hincrbyfloat("count_fees_days", $this->days_since_1970($now), $fees);
        $this->redis->hincrbyfloat("count_fees_month", $this->months_since_1970($now), $fees);
        $this->redis->incrbyfloat("count_fees", $fees);

        $this->add_to_revenue($fees);
    }

    public function add_to_revenue($float)
    {
        $now = microtime(true);

        $this->redis->hincrbyfloat("count_revenue_minutes", $this->minutes_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_revenue_hours", $this->hours_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_revenue_days", $this->days_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_revenue_month", $this->months_since_1970($now), $float);
        $this->redis->incrbyfloat("count_revenue", $float);
    }


    /**
     * Use to remove the fees that we taken, in case of refund
     * @param Sale $sale
     * @param $fees
     */
    public function add_refund_fee(\Ystos\Common\Entity\Sale $sale,$float)
    {
        $now = $sale->getDate()->getTimestamp();

        $this->redis->hincrbyfloat("count_refund_fees_minutes", $this->minutes_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_refund_fees_hours", $this->hours_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_refund_fees_days", $this->days_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_refund_fees_month", $this->months_since_1970($now), $float);
        $this->redis->incrbyfloat("count_refund_fees", $float);

        $this->add_refund_revenue($sale,$float);
    }

    public function add_refund_revenue(Sale $sale,$float){
        $now = $sale->getDate()->getTimestamp();

        $this->redis->hincrbyfloat("count_refund_revenue_minutes", $this->minutes_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_refund_revenue_hours", $this->hours_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_refund_revenue_days", $this->days_since_1970($now), $float);
        $this->redis->hincrbyfloat("count_refund_revenue_month", $this->months_since_1970($now), $float);
        $this->redis->incrbyfloat("count_refund_revenue", $float);
    }

    /**
     * @param $product Product
     */
    public function remove_pending_sale(Product $product)
    {
        $date_added = $product->getOrderDate()->getTimestamp();

        $this->redis->hincrby("count_sale_pending_minutes", $this->minutes_since_1970($date_added), 1);
        $this->redis->hincrby("count_sale_pending_hours", $this->hours_since_1970($date_added), 1);
        $this->redis->hincrby("count_sale_pending_days", $this->days_since_1970($date_added), 1);
    }

    public function add_location_visitor($country_code)
    {
        $now = microtime(true);
        $this->redis->hincrby("count_visitor_" . $country_code, $this->days_since_1970($now), 1);
    }

    public function add_product(Product $product){

    }

    public function add_location_registered_user($country_code)
    {
        $now = microtime(true);
        $this->redis->hincrby("count_user_" . $country_code, $this->days_since_1970($now), 1);
    }

    public function get_pending_sale_24_hours()
    {
        $now = microtime(true);
        return $this->redis->hget("count_sale_pending_days", $this->days_since_1970($now));
    }

    public function get_last_hour_user_registered()
    {
        $now = microtime(true);
        $array = array();

        $array_labels = array();
        $min = 0;
        $user_registered_last_5_m = 0;
        for ($i = $this->minutes_since_1970($now); $i >= $this->minutes_since_1970($now) - 60; $i--) {
            $user_registered_last_5_m += intval($this->redis->hget("count_user_minutes", $i));
            //if($i%5 == 0){
            $array_labels[] = $min . ' m';
            $array[] = $user_registered_last_5_m;
            $user_registered_last_5_m = 0;
            //}
            $min++;
        }

        return ['labels' => array_reverse($array_labels), 'datasets' => [
            [
                'label' => "Users Registered",
                'fillColor' => "rgba(60,141,188,0.9)",
                'strokeColor' => "rgba(60,141,188,0.8)",
                'pointColor' => "#3b8bba",
                'pointStrokeColor' => "rgba(60,141,188,1)",
                'pointHighlightFill' => "#fff",
                'pointHighlightStroke' => "rgba(60,141,188,1)",
                'data' => array_reverse($array)]
        ]];
    }

    public function get_last_24_h_user_registered()
    {
        $now = microtime(true);

        return $this->redis->hget("count_user_days", $this->days_since_1970($now));
    }

    public function get_total_sales()
    {
        return $this->redis->get("count_sale_completed");
    }

    function months_since_1970($end_ts)
    {
        $d2 = new \DateTime();
        $d2->setTimestamp($end_ts);
        $d1 = new \DateTime("1970-01-01 00:00:00");
        return $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
    }

    function days_since_1970($end_ts)
    {
        return round($end_ts / 86400);
    }

    function minutes_since_1970($end_ts)
    {
        return round($end_ts / (60));
    }

    function hours_since_1970($end_ts)
    {
        return round($end_ts / (60 * 60));
    }
}