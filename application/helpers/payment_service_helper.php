<?php
namespace Ystos\Common\Service;

include_once COMMONPATH . 'models/Entity/Payment/Mangopay_Information.php';

use MangoPay\Address;
use MangoPay\BankAccount;
use MangoPay\Libraries\Exception;
use MangoPay\Libraries\ResponseException;
use MangoPay\MangoPayApi;
use MangoPay\Money;
use MangoPay\UserLegal;
use MangoPay\UserNatural;
use MangoPay\Wallet;
use Ystos\Common\Entity\Payment\Mangopay_Information;
use Ystos\Common\Entity\Professional\Legal_Information;
use Ystos\Common\Entity\Professional\Shop;

/**
 * Service that handle the in-two-time payment system
 *
 * In this class all the variable prefixed with "m" is value provided by mangopay
 *
 * @author Etienne Pichereau <pichereau.e@gmail.com>
 */
class Payment
{
    public $_api;

    public $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        /**
         * Initialize MangoPay API
         */
        $this->_api = new MangoPayApi();
        $this->_api->Config->ClientId = $this->CI->config->item('mangopay_client_id');
        $this->_api->Config->ClientPassword = $this->CI->config->item('mangopay_password');
        $this->_api->Config->TemporaryFolder = $this->CI->config->item('mangopay_tmp_folder');
        $this->_api->Config->BaseUrl = $this->CI->config->item('mangopay_base_url');
        $this->_api->Config->DebugMode = $this->CI->config->item('mangopay_debug');
    }


    /**
     * Create a natural user
     * @return \MangoPay\User | boolean
     */
    public function createNaturalUser($first_name, $last_name, $birthday, $nationality, $residence_country, $email)
    {
        $user = new UserNatural();
        $user->PersonType = 'NATURAL';
        $user->FirstName = $first_name;
        $user->LastName = $last_name;
        $user->Birthday = $birthday;
        $user->Nationality = $nationality;
        $user->CountryOfResidence = $residence_country;
        $user->Email = $email;


        try {
            //Send the request
            $user = $this->_api->Users->Create($user);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
            // handle/log the exception $e->GetMessage()
        }
        return $user;
    }


    /**
     * Create a selling wallet
     * @return \MangoPay\Wallet | boolean
     */
    public function create_product_selling_wallet($mangopay_user_id, $currency = null)
    {
        $user = new UserNatural();

        $wallet = new Wallet();

        $wallet->Currency = $this->getCurrency($currency);
        $wallet->Owners = [$mangopay_user_id];
        $wallet->Description = 'Sells';


        try {
            //Send the request
            return $this->_api->Wallets->Create($wallet);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
            // handle/log the response exception with code $e->GetCode(), message $e->GetMessage() and error(s) $e->GetErrorDetails()
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
            // handle/log the exception $e->GetMessage()
        }
    }


    /**
     * Create a complete natural user ( user information, address and account )
     *
     * To see how to use $user_data variable please check the function
     * @param $user_data
     *
     * @return array | boolean
     *
     */
    public function createCompleteNaturalUser($user_data, $id = null)
    {

        // Build the user object
        $user = new UserNatural();
        $user->PersonType = 'NATURAL';
        $user->FirstName = $user_data['first_name'];
        $user->LastName = $user_data['last_name'];
        // The birthday is a timestamp value
        $user->Birthday = $user_data['birthday'];
        $user->Nationality = $user_data['nationality'];
        $user->CountryOfResidence = $user_data['residence_country'];
        $user->Email = $user_data['email'];

        // Build the address object
        $address = new Address();
        $address->AddressLine1 = $user_data['address']['address_line_1'];
        $address->AddressLine2 = $user_data['address']['address_line_2'];
        $address->City = $user_data['address']['city'];
        $address->Country = $user_data['address']['country'];
        $address->PostalCode = $user_data['address']['postal_code'];
        $address->Region = $user_data['address']['region'];

        // Inject the address in the user
        $user->Address = $address;

        try {
            if ($id===null){
                $user = $this->_api->Users->Create($user);
            }
            else{
                $user->Id = $id;
                $user = $this->_api->Users->Update($user);
            }
            // Save the user
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        }

        // Build the account object
        $account = new \MangoPay\BankAccount();
        $account->OwnerName = $user->FirstName . ' ' . $user->LastName;
        $account->OwnerAddress = $user->Address;
        $account->Details = new \MangoPay\BankAccountDetailsIBAN();
        $account->Details->IBAN = $user_data['account']['iban'];
        $account->Details->BIC = $user_data['account']['bic'];

        try {
            // Add the bank Account
            $this->_api->Users->CreateBankAccount($user->Id, $account);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        }

        // Create wallet for seller user
        $wallet = new Wallet();
        $wallet->Owners = array($user->Id);
        $wallet->Currency = $this->getCurrency($user_data['currency']);
        $wallet->Description = 'Ventes produits';

        try {
            // Create a wallet for all the sales
            $m_wallet_id = $this->_api->Wallets->Create($wallet);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        }

        return ['m_user' => $user, 'm_wallet_id' => $m_wallet_id->Id];
    }


    /**
     * Create a legal user ( company )
     * @return array | boolean
     */
    public function create_complete_legal_user(Shop $shop, $iban, $bic, $currency = 'EUR')
    {
        // TODO Integration of company in v2
        $user = new UserLegal();
        $user->HeadquartersAddress = new Address();
        $user->HeadquartersAddress->AddressLine1 = $shop->getLegalInformation()->getHeadquarterAddressLine1();
        $user->HeadquartersAddress->AddressLine2 = $shop->getLegalInformation()->getHeadquarterAddressLine1();
        $user->HeadquartersAddress->City = $shop->getLegalInformation()->getHeadquarterCity();
        $user->HeadquartersAddress->Country = $shop->getLegalInformation()->getHeadquarterCountry();
        $user->HeadquartersAddress->PostalCode = $shop->getLegalInformation()->getHeadquarterPostalcode();
        $user->HeadquartersAddress->Region = $shop->getLegalInformation()->getHeadquarterRegion();
        $user->Name = $shop->getName();
        $user->Email = $shop->getEmail();
        $user->LegalPersonType = 'BUSINESS';
        $user->LegalRepresentativeFirstName = $shop->getLegalInformation()->getRepresentativeFirstname();
        $user->LegalRepresentativeLastName = $shop->getLegalInformation()->getRepresentativeLastname();
        /*$user->LegalRepresentativeAddress = new Address();
        $user->LegalRepresentativeAddress->AddressLine1 = $company_info['legal_representative']['address_line_1'];
        $user->LegalRepresentativeAddress->AddressLine2 = $company_info['legal_representative']['address_line_2'];
        $user->LegalRepresentativeAddress->City = $company_info['legal_representative']['city'];
        $user->LegalRepresentativeAddress->Country = $company_info['legal_representative']['country_code'];
        $user->LegalRepresentativeAddress->PostalCode = $company_info['legal_representative']['postal_code'];
        $user->LegalRepresentativeAddress->Region = $company_info['legal_representative']['region'];*/
        $user->LegalRepresentativeBirthday = $shop->getLegalInformation()->getRepresentativeBirthday()->getTimestamp();
        $user->LegalRepresentativeNationality = $shop->getLegalInformation()->getRepresentativeNationality();
        $user->LegalRepresentativeCountryOfResidence = $shop->getLegalInformation()->getRepresentativeCountryOfResidence();

        try {
            // Save the user
            $user = $this->_api->Users->Create($user);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the legal user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the legal user: ' . $e->GetMessage());
            return false;
        }

        // Build the account object
        $account = new \MangoPay\BankAccount();
        $account->OwnerName = $user->Name;
        $account->OwnerAddress = $user->HeadquartersAddress;
        $account->Details = new \MangoPay\BankAccountDetailsIBAN();
        $account->Details->IBAN = $iban;
        $account->Details->BIC = $bic;

        try {
            // Add the bank Account
            $this->_api->Users->CreateBankAccount($user->Id, $account);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        }

        // Create wallet for seller user
        $wallet = new \MangoPay\Wallet();
        $wallet->Owners = array($user->Id);
        $wallet->Currency = $this->getCurrency(isset($currency ) ? $currency : null);
        $wallet->Description = 'Sells Services';

        try {
            // Create a wallet for all the sales
            $m_wallet_id = $this->_api->Wallets->Create($wallet);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the natural user: ' . $e->GetMessage());
            return false;
        }

        return ['m_user' => $user, 'm_wallet_id' => $m_wallet_id->Id];

    }

    /**
     * Add a complete address to the user
     * This is mandatory to register bank account
     *
     * @param $m_user_id
     * @param $address_line_1
     * @param $address_line_2
     * @param $city
     * @param $country ISO 3166-1 alpha-2 (FR, GB, US .... )
     * @param string $postal_code
     * @param string $region
     * @return \MangoPay\UserLegal|\MangoPay\UserNatural | boolean
     */
    public function addAddress($m_user_id, $address_line_1, $address_line_2, $city, $country, $postal_code = '', $region = '')
    {
        $address = new \MangoPay\Address();
        $address->AddressLine1 = $address_line_1;
        $address->AddressLine2 = $address_line_2;
        $address->City = $city;
        $address->Country = $country;
        $address->PostalCode = $postal_code;
        $address->Region = $region;

        try {
            // Get the user on mangopay
            $user = $this->_api->Users->Get($m_user_id);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the user: ' . $e->GetMessage());
            return false;
        }
        $user->Address = $address;

        try {
            // Update the user information
            $this->_api->Users->Update($user);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot update the user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot update the user: ' . $e->GetMessage());
            return false;
        }
        return $user;
    }


    /**
     * Add a bank account to a user
     * The user need to have register an exact address
     *
     * @param $m_user_id
     * @param $iban
     * @param $bic
     * @return \MangoPay\BankAccount | boolean
     */
    public function addBankAccount($m_user_id, $iban, $bic)
    {

        try {
            // Get the user on mangopay
            $user = $this->_api->Users->Get($m_user_id);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the user: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the user: ' . $e->GetMessage());
            return false;
        }

        // Build the account object
        $account = new \MangoPay\BankAccount();
        $account->OwnerName = $user->FirstName . ' ' . $user->LastName;
        $account->OwnerAddress = $user->Address;
        $account->Details = new \MangoPay\BankAccountDetailsIBAN();
        $account->Details->IBAN = $iban;
        $account->Details->BIC = $bic;

        try {
            // Send request to mangopay
            $bank_account = $this->_api->Users->CreateBankAccount($user->Id, $account);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot add the bank account: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot add the bank account: ' . $e->GetMessage());
            return false;
        }

        return $bank_account;
    }

    /**
     * Create a CardRegistration object used in the process to register a card for a user
     *
     * @param $m_user_id
     * @param $currency
     * @return \MangoPay\CardRegistration | boolean
     */
    public function startCardRegistration($m_user_id, $currency = null)
    {

        // Register the card
        $card_register = new \MangoPay\CardRegistration();
        $card_register->UserId = $m_user_id;
        $card_register->Currency = $this->getCurrency($currency);
        // TODO HAndle the others card type
        //$card_register->CardType =

        try {
            return $this->_api->CardRegistrations->Create($card_register);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot start card registration: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot start card registration: ' . $e->GetMessage());
            return false;
        }

    }


    /**
     * Not used
     *
     * @param $card_register_id
     * @param $data
     * @param $amount
     * @param $currency
     * @return array|\MangoPay\PayIn
     */
    /*public function pay($card_register_id, $data, $amount, $currency = null)
    {

        $card_register = $this->_api->CardRegistrations->Get($card_register_id);

        if ($card_register->Status != 'VALIDATED' || !isset($card_register->CardId))
            return ['status' => false, 'message' => 'Cannot create card. Payment has not been created.'];

        // get created virtual card object
        $card = $this->_api->Cards->Get($card_register->CardId);

        // create temporary wallet for user
        $wallet = new \MangoPay\Wallet();
        $wallet->Owners = array($card_register->UserId);
        $wallet->Currency = $this->getCurrency($currency);
        $wallet->Description = 'Temporary wallet for payment demo';
        $created_wallet = $this->_api->Wallets->Create($wallet);

        // create pay-in CARD DIRECT
        $pay_in = new \MangoPay\PayIn();
        $pay_in->CreditedWalletId = $created_wallet->Id;
        $pay_in->AuthorId = $card_register->UserId;
        $pay_in->DebitedFunds = new \MangoPay\Money();
        $pay_in->DebitedFunds->Amount = $amount;
        $pay_in->DebitedFunds->Currency = $this->getCurrency($currency);
        $pay_in->Fees = new \MangoPay\Money();
        $pay_in->Fees->Amount = 0;
        $pay_in->Fees->Currency = $this->getCurrency($currency);

        // payment type as CARD
        $pay_in->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
        $pay_in->PaymentDetails->CardType = $card->CardType;
        $pay_in->PaymentDetails->CardId = $card->Id;
        // execution type as DIRECT
        $pay_in->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
        $pay_in->ExecutionDetails->SecureModeReturnURL = $this->config['payment_callback_url'];
        // create Pay-In
        return $this->_api->PayIns->Create($pay_in);

    }*/

    /**
     * Create a preAuthorization for the buyer card
     *
     * @param $card_register_id
     * @param $amount
     * @param null $currency
     * @return array|\MangoPay\CardPreAuthorization | boolean
     */
    public function preAuthoriseTransaction($card_register_id, $amount, $currency = null, $data = null)
    {
        try {
            // Get information about the card registration
            $card_register = $this->_api->CardRegistrations->Get($card_register_id);
            if($data !== null && $card_register->RegistrationData === null){
                $card_register->RegistrationData = isset($data['RegistrationData']) ? $data['RegistrationData'] : ''  ;
                $card_register = $this->_api->CardRegistrations->Update($card_register);
            }

        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the card registration information: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the card registration information: ' . $e->GetMessage());
            return false;
        }


        // Check if the card status is good
        if ($card_register->Status != 'VALIDATED' || !isset($card_register->CardId))
            return ['status' => false, 'message' => 'Cannot create card. Payment has not been created.'];

        // Create the preAuthorization object
        $card_pre_authorization = new \MangoPay\CardPreAuthorization();
        $card_pre_authorization->AuthorId = $card_register->UserId;
        $card_pre_authorization->DebitedFunds = new \MangoPay\Money();
        $card_pre_authorization->DebitedFunds->Currency = $this->getCurrency($currency);

        log_message('info', 'PreAuth: Fees =>' . $this->calculateFees($amount));

        $card_pre_authorization->DebitedFunds->Amount = $amount + $this->calculateFees($amount);
        $card_pre_authorization->CardId = $card_register->CardId;
        $card_pre_authorization->SecureModeReturnURL = $this->CI->config->item('mangopay_payment_callback_url');


        try {
            // Save the preAuthorization
            return $this->_api->CardPreAuthorizations->Create($card_pre_authorization);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the pre authorization: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the pre authorization: ' . $e->GetMessage());
            return false;
        }
    }

    /**
     * Create a payIn
     *
     * @param $card_register_id
     * @param $amount
     * @param null $currency
     * @return array|\MangoPay\PayIn | boolean
     */
    public function payInTransaction($card_register_id,Legal_Information $legal_Information, $amount, $currency = null, $data = null)
    {
        try {
            // Get information about the card registration
            $card_register = $this->_api->CardRegistrations->Get($card_register_id);
            if($data !== null && $card_register->RegistrationData === null){
                $card_register->RegistrationData = isset($data['RegistrationData']) ? $data['RegistrationData'] : ''  ;
                $card_register = $this->_api->CardRegistrations->Update($card_register);
            }

        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the card registration information: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the card registration information: ' . $e->GetMessage());
            return false;
        }

        try {
            // Get the wallet of the seller
            $m_seller_wallet = $this->_api->Wallets->Get($legal_Information->getMangopaySellerWalletId());
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the seller wallet: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the seller wallet: ' . $e->GetMessage());
            return false;
        }


        // Check if the card status is good
        if ($card_register->Status != 'VALIDATED' || !isset($card_register->CardId))
            return ['status' => false, 'message' => 'Cannot create card. Payment has not been created.'];

        // Create the preAuthorization object
        $payin = new \MangoPay\PayIn();
        $payin->AuthorId = $card_register->UserId;
        $payin->CreditedWalletId = $m_seller_wallet->Id;
        $payin->DebitedFunds = new \MangoPay\Money();
        $payin->DebitedFunds->Currency = $this->getCurrency($currency);
        $payin->DebitedFunds->Amount = $amount + $this->calculateFees($amount);
        $payin->Fees = new \MangoPay\Money();
        $payin->Fees->Amount = 0;
        $payin->Fees->Currency = $this->getCurrency($currency);

        $payin->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
        $payin->PaymentDetails->CardId = $card_register->CardId;
        // execution type as DIRECT
        $payin->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
        $payin->ExecutionDetails->SecureModeReturnURL = $this->CI->config->item('mangopay_payment_callback_url');


        try {
            // Save the payIn
            return $this->_api->PayIns->Create($payin);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the payin: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the payin: ' . $e->GetMessage());
            return false;
        }
    }

    /**
     * Finish the in-two-time transaction
     *
     * @param \Ystos\Common\Entity\Payment\Mangopay_Information $mangopay_Information
     * @param $pre_authorization_id
     * @param $amount
     * @param null $currency
     * @return \MangoPay\PayIn | boolean
     */
    public function confirmTransaction(\Ystos\Common\Entity\Payment\Mangopay_Information $mangopay_Information, $pre_authorization_id, $amount, $product_id,$currency = null)
    {
        try {
            // Get the wallet of the seller
            $m_seller_wallet = $this->_api->Wallets->Get($mangopay_Information->getMangopaySellerWalletId());
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the seller wallet: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the seller wallet: ' . $e->GetMessage());
            return false;
        }

        try {
            // Get the preAuthorization previously created in the first phase of the payment
            $card_pre_authorization = $this->_api->CardPreAuthorizations->Get($pre_authorization_id);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the pre authorization: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the pre authorization: ' . $e->GetMessage());
            return false;
        }

        try {
            $card = $this->_api->Cards->Get($card_pre_authorization->CardId);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the buyer card: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the buyer card: ' . $e->GetMessage());
            return false;
        }

        // TODO Check Expiration Date

        // Create the payin object, it's used to send money on wallet
        $pay_in = new \MangoPay\PayIn();
        $pay_in->Tag = '{"pid" : '.$product_id.'}';
        $pay_in->CreditedWalletId = $m_seller_wallet->Id;
        $pay_in->AuthorId = $card->UserId;
        $pay_in->DebitedFunds = new \MangoPay\Money();
        $pay_in->DebitedFunds->Amount = $amount + $this->calculateFees($amount);
        $pay_in->DebitedFunds->Currency = $this->getCurrency($currency);

        // Fee
        $pay_in->Fees = new \MangoPay\Money();
        // We add our fees
        $pay_in->Fees->Amount = $this->calculateFees($amount);
        $pay_in->Fees->Currency = $currency;

        // payment type as CARD
        /*$pay_in->PaymentDetails = new \MangoPay\PayInPaymentDetailsCard();
        $pay_in->PaymentDetails->CardType = $card->CardType;
        $pay_in->PaymentDetails->CardId = $card->Id;*/

        $pay_in->PaymentDetails = new \MangoPay\PayInPaymentDetailsPreAuthorized();
        $pay_in->PaymentDetails->PreauthorizationId = $pre_authorization_id;
        // execution type as DIRECT
        $pay_in->ExecutionDetails = new \MangoPay\PayInExecutionDetailsDirect();
        $pay_in->ExecutionDetails->SecureModeReturnURL = $this->CI->config->item('mangopay_payment_callback_url');

        try {
            // Save the payin object, send the money on the seller wallet
            return $this->_api->PayIns->Create($pay_in);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot send the money on the seller wallet: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot send the money on the seller waller ' . $e->GetMessage());
            return false;
        }
    }


    public function list_user_wallet_transactions(Mangopay_Information $mangopay_Information){

        try {
            return $this->_api->Wallets->GetTransactions($mangopay_Information->getMangopaySellerWalletId());
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get wallet transactions: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get wallet transactions: ' . $e->GetMessage());
            return false;
        }
    }

    public function get_wallet_balance(Mangopay_Information $mangopay_Information){
        try {
            return $this->_api->Wallets->Get($mangopay_Information->getMangopaySellerWalletId())->Balance->Amount / 100;
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get wallet transactions: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get wallet transactions: ' . $e->GetMessage());
            return false;
        }
    }

    public function get_accounts(Mangopay_Information $mangopay_Information){
        try {
            return $this->_api->Users->GetBankAccounts($mangopay_Information->getMangopayUserId());
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get wallet transactions: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get wallet transactions: ' . $e->GetMessage());
            return false;
        }
    }


    public function payout(Mangopay_Information $mangopay_Information){
        try {

            $bank_accounts = $this->_api->Users->GetBankAccounts($mangopay_Information->getMangopayUserId());

            if(isset($bank_accounts[0])) {
                /**
                 * @var $bank_account BankAccount
                 */
                $bank_account = $bank_accounts[0];
            }
            else{
                return false;
            }
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get bank accounts: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get bank accounts: ' . $e->GetMessage());
            return false;
        }

        return $this->payOutOnAccount($mangopay_Information->getMangopaySellerWalletId(), $bank_account->Id);
    }


    /**
     *
     * Get a PreAuthorization object in Mango., / m.n,n n jkklj;mn;#4
     * @param $pre_authorization_id
     * @return \MangoPay\CardPreAuthorization | boolean
     */
    public function getPreAuthorization($pre_authorization_id)
    {
        try {
            $card_pre_authorization = $this->_api->CardPreAuthorizations->Get($pre_authorization_id);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot create the pre authorization: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot create the pre authorization: ' . $e->GetMessage());
            return false;
        }

        return $card_pre_authorization;
    }

    /**
     * Send the money on the seller wallet on his bank account
     *
     * @param $debited_wallet_id
     * @param $account_id
     * @return \MangoPay\PayOut | boolean
     */
    public function payOutOnAccount($debited_wallet_id, $account_id)
    {
        try {
            // Get the seller wallet
            $wallet = $this->_api->Wallets->Get($debited_wallet_id);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot get the seller wallet: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot get the seller wallet: ' . $e->GetMessage());
            return false;
        }

        // Create the payout object, used to send money form wallet to bank account
        $pay_out = new \MangoPay\PayOut();
        $pay_out->Tag = 'DefaultTag';
        $pay_out->AuthorId = $wallet->Owners[0];
        $pay_out->CreditedUserId = $wallet->Owners[0];
        $pay_out->DebitedFunds = new \MangoPay\Money();
        $pay_out->DebitedFunds->Currency = $wallet->Balance->Currency;
        $pay_out->DebitedFunds->Amount = $wallet->Balance->Amount;
        $pay_out->Fees = new Money();
        $pay_out->Fees->Currency = $wallet->Balance->Currency;
        $pay_out->Fees->Amount = 0;
        $pay_out->DebitedWalletId = $wallet->Id;

        $pay_out->MeanOfPaymentDetails = new \MangoPay\PayOutPaymentDetailsBankWire();
        $pay_out->MeanOfPaymentDetails->BankAccountId = $account_id;
        $pay_out->MeanOfPaymentDetails->BankWireRef = $this->CI->config->item('mangopay_bank_ref');

        try {
            // Send the money on the seller bank account
            return $this->_api->PayOuts->Create($pay_out);
        } catch (ResponseException $e) {
            log_message('error', 'Mangopay | Cannot send the money on the seller bank account: ' . $e->GetMessage());
            return false;
        } catch (Exception $e) {
            log_message('error', 'Mangopay | Cannot send the money on the seller bank account: ' . $e->GetMessage());
            return false;
        }
    }

    /**
     * Calculate the fees
     * @param $amount
     * @return float|int
     */
    public function calculateFees($amount)
    {
        $price = floatval($amount);
        return  $this->CI->config->item('commission_function')($price) * $price;
    }

    /**
     * Get the currency, if not defined it return the default currency
     * @param $currency
     * @return mixed
     */
    private function getCurrency($currency)
    {
        return isset($currency) && $currency !== null && $currency !== '' ? $currency : $this->CI->config->item('mangopay_default_currency');
    }
}