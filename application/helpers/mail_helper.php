<?php
namespace Ystos\Common\Service;

use \Mailjet\Resources;

use Ystos\Common\Entity\Messaging\Conversation;
use Ystos\Common\Entity\Payment\Offer;
use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\Sale;
use Ystos\Common\Entity\User;

class Mail
{

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('language');
        $this->CI->lang->load('mail');
        $this->CI->load->helper('url');
        $config = [
            'functions' => ['lang'],
        ];
        $this->CI->load->library('twig', $config);
        $this->CI->twig->addGlobal('config', $this->CI->config);
    }

    /**
    /**
     * Send an email to remind the customer to get his purchased product
     * @param array $recipients
     *
     * @return bool
     */
    public function send_only_three_day_left(array $recipients)
    {
        $html_part = $this->CI->twig->render('mail/sells/only_three_days_left');
        $subject = $this->CI->lang->line('mail_only_three_days_left_subject');
        return $this->buildMail($recipients, $subject, $html_part);
    }


    /**
     * /**
     * Send an email to the buyer to rate the seller
     * @param Sale $sale
     *
     * @return bool
     */
    public function send_rate_seller(Sale $sale)
    {
        $html_part = $this->CI->twig->render('mail/sells/rate_seller',
            array(
                'first_name' => $sale->getBuyer()->getFirstName(),
                'product_title' => $sale->getProduct()->getTitle(),
                'feedback_token' => $sale->getUserFeedback()->getToken()
            ));
        $subject = sprintf($this->CI->lang->line('mail_rate_seller_subject'), $sale->getBuyer()->getFirstName(), $sale->getProduct()->getTitle());

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$sale->getBuyer()->getEmail()], $subject, $html_part, $header_image);
    }

    /**
     * /**
     * Send an email to the buyer to informe that the offer is rejected by the seller
     * @param User $user
     *
     * @return bool
     */
    public function send_mail_validation(User $user, $token)
    {
        $html_part = $this->CI->twig->render('mail/user/mail_validation',
            array(
                'first_name' => $user->getFirstName(),
                'token' => $token,
            ));
        $subject = sprintf($this->CI->lang->line('mail_validation_subject'), $user->getFirstName());

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$user->getEmail()], $subject, $html_part, $header_image);
    }

    /**
     * /**
     * Send an email to the user for reset his password
     * @param User $user
     *
     * @return bool
     */
    public function send_forgot_password(User $user, $token)
    {
        $html_part = $this->CI->twig->render('mail/user/forgot_password',
            array(
                'first_name' => $user->getFirstName(),
                'token' => $token,
                'email' => $user->getEmail()
            ));
        $subject = sprintf($this->CI->lang->line('forgot_password_subject'), $user->getFirstName());

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$user->getEmail()], $subject, $html_part, $header_image);
    }

    /**
     * /**
     * Send an email to the buyer to informe that the offer is rejected by the seller
     * @param User $user
     *
     * @return bool
     */
    public function send_welcome_mail(User $user)
    {
        $html_part = $this->CI->twig->render('mail/user/welcome',
            array(
                'first_name' => $user->getFirstName(),
            ));
        $subject = sprintf($this->CI->lang->line('mail_welcome_subject'), $user->getFirstName());

        $header_image = 'https://res.cloudinary.com/ystos-com/image/upload/c_scale,q_70,w_800/v1504431989/camera-581126_1920_mmobim.jpg';
        return $this->buildMail([$user->getEmail()], $subject, $html_part, $header_image, null, TRUE);
    }

    /**
     * /**
     * Send an email to the buyer to informe that the offer is rejected by the seller
     * @param Offer $offer
     *
     * @return bool
     */
    public function send_offer_rejected(Offer $offer)
    {
        $html_part = $this->CI->twig->render('mail/offer/rejected_offer',
            array(
                'first_name' => $offer->getUser()->getFirstName(),
                'product_title' => $offer->getClassified()->getTitle(),
            ));
        $subject = sprintf($this->CI->lang->line('mail_rejected_offer_subject'), $offer->getUser()->getFirstName());

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$offer->getUser()->getEmail()], $subject, $html_part, $header_image);
    }

    /**
     *
     * Send an email to the seller to inform there is a new offer for his product
     * @param Offer $offer
     *
     * @return bool
     */
    public function send_new_offer_seller(Product $product, User $user)
    {
        $html_part = $this->CI->twig->render('mail/offer/new_offer_seller',
            array(
                'first_name' => $product->getOwner()->getFirstName(),
                'product_id' => $product->getId(),
                'phone' => $user->getPhone()
            ));
        $subject = $this->CI->lang->line('mail_new_offer_seller_subject');

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$product->getOwner()->getEmail()], $subject, $html_part, $header_image);
    }
    /**
     *
     * Send an email to the buyer to repeat all the important information about the sale
     * @param Offer $offer
     *
     * @return bool
     */
    public function send_new_offer_buyer(Offer $offer,User $seller)
    {
        $html_part = $this->CI->twig->render('mail/offer/new_offer_buyer',
            array(
                'first_name' => $offer->getUser()->getFirstName(),
                'validation_code' => $offer->getTransaction()['code'],
                'phone' => $seller->getPhone()
            ));
        $subject = $this->CI->lang->line('mail_new_offer_buyer_subject');

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$offer->getUser()->getEmail()], $subject, $html_part, $header_image);
    }
    
    public function send_disable_offer(Offer $offer){
        $this->send_disable_offer_seller($offer);
        
        return $this->send_disable_offer_buyer($offer);
        
    }
    
    public function send_disable_offer_seller(Offer $offer){
        $html_part = $this->CI->twig->render('@common/mail/offer/disable_offer',
            array(
                'first_name' => $offer->getClassified()->getOwner()->getFirstName(),
                'product_title' => $offer->getClassified()->getTitle()
            ));
        $subject = 'Offre expirée';

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$offer->getClassified()->getOwner()->getEmail()], $subject, $html_part, $header_image);
    }
    
    public function send_disable_offer_buyer(Offer $offer){
        $html_part = $this->CI->twig->render('@common/mail/offer/disable_offer',
            array(
                'first_name' => $offer->getUser()->getFirstName(),
                'product_title' => $offer->getClassified()->getTitle()
            ));
        $subject = 'Offre expirée';

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$offer->getUser()->getEmail()], $subject, $html_part, $header_image);
    }
    
    public function send_reminder_offer_3_days(Offer $offer){
        $this->send_reminder_offer_3_days_user($offer, $offer->getUser(), $offer->getClassified()->getOwner()->getPhone());
        return $this->send_reminder_offer_3_days_user($offer, $offer->getClassified()->getOwner(), $offer->getUser()->getPhone());
    }
    
    public function send_reminder_offer_3_days_user(Offer $offer, User $user, $phone){
        $html_part = $this->CI->twig->render('@common/mail/offer/reminder_3_days',
            array(
                'first_name' => $user->getFirstName(),
                'phone' => $phone,
                'product_title' => $offer->getClassified()->getTitle()
            ));
        $subject = 'Plus que 4 jours pour valider la vente';

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$user->getEmail()], $subject, $html_part, $header_image);
    }
    
    public function send_reminder_offer_5_days(Offer $offer){
        $this->send_reminder_offer_3_days_user($offer, $offer->getUser(),$offer->getClassified()->getOwner()->getPhone());
        return $this->send_reminder_offer_3_days_user($offer, $offer->getClassified()->getOwner(), $offer->getUser()->getPhone());
    }
    
    public function send_reminder_offer_5_days_user(Offer $offer, User $user, $phone){
        $html_part = $this->CI->twig->render('@common/mail/offer/reminder_5_days',
            array(
                'first_name' => $user->getFirstName(),
                'phone' => $phone,
                'product_title' => $offer->getClassified()->getTitle()
            ));
        $subject = 'Moins de 2 jours pour valider la vente !!';

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$user->getEmail()], $subject, $html_part, $header_image);
    }
    
        
    public function send_reminder_bank_account(Product $product){
        $html_part = $this->CI->twig->render('@common/mail/user/add_bank_account',
            array(
                'first_name' => $product->getOwner()->getFirstname(),
            ));
        $subject = 'Nous avons besoin de tes coordonnées bancaires';

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$product->getOwner()->getEmail()], $subject, $html_part, $header_image);
    }
    

    

    /**
     *
     * Send an email to the buyer to notify it there is a new message on the platform
     *
     * @param $conversation \Ystos\Common\Entity\Messaging\Conversation
     *
     * @return bool
     */
    public function send_new_message_seller(Conversation $conversation)
    {
        $html_part = $this->CI->twig->render('mail/message/new_message_seller',
            array(
                'sender_first_name' => $conversation->getUser()->getFirstName(),
                'receiver_first_name' => $conversation->getOwner()->getFirstName(),
                'product_title' => $conversation->getProduct()->getTitle(),
                'message_token' => $conversation->getToken()
            ));
        $subject = sprintf($this->CI->lang->line('mail_new_message_seller_subject'), $conversation->getOwner()->getFirstName());

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$conversation->getOwner()->getEmail()], $subject, $html_part, $header_image);
    }

    /**
     *
     * Send an email to the user whose interested by the produuct to notify it there is a new message on the platform
     *
     * @param $conversation Conversation
     *
     * @return bool
     */
    public function send_new_message_interested_user(Conversation $conversation)
    {
        $html_part = $this->CI->twig->render('mail/message/new_message_interested_user',
            array(
                'sender_first_name' => $conversation->getOwner()->getFirstName(),
                'receiver_first_name' => $conversation->getUser()->getFirstName(),
                'product_title' => $conversation->getProduct()->getTitle(),
                'message_token' => $conversation->getToken()
            ));
        $subject = sprintf($this->CI->lang->line('mail_new_message_interested_user_subject'), $conversation->getUser()->getFirstName());

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail([$conversation->getUser()->getEmail()], $subject, $html_part, $header_image);
    }

    /**
     *
     * Send an email to the user whose interested by the produuct to notify it there is a new message on the platform
     *
     *
     * @return bool
     */
    public function send_contact_form_message($message, $email, $firstname=null, $lastname=null, $subject=null)
    {
        $html_part = $this->CI->twig->render('mail/contact',
            array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'message' => $message,
                'email' => $email
            ));

        if(!empty($subject)){
            $subject = '[CONTACT] '.$subject;
        }
        else{
            $subject = 'Nouveau message via le Ystos.com';
        }

        $header_image = 'http://res.cloudinary.com/ystos-com/image/upload/c_lfill,g_south,h_200,w_560/v1506697594/pexels-photo-318651_copie_iruz1f.jpg';
        return $this->buildMail(['etienne.pichereau@ystos.com'], $subject, $html_part, $header_image);
    }

    /**
     *
     * Build the MailJet mail, and send it
     *
     * @param $recipients
     * @param $subject
     * @param $html_part
     * @param null $text_part
     * @return bool
     */
    public function buildMail($recipients, $subject, $html_part, $header_image = null, $text_part = null, $empty_layout = false, $sender_name = 'Ystos', $sender_email = 'hello@ystos.com')
    {

        $this->CI =& get_instance();

        $company_name = $this->CI->config->item('company_name');
        $socials = $this->CI->config->item('social');
        $contact_address = $this->CI->config->item('contact_address');
        $mail_logo = $this->CI->config->item('mail_logo');

        $html_part = $this->CI->twig->render('@common/mail/template/layout',
            [
                'subject' => $subject,
                'content' => $html_part,
                'company_name' => $company_name,
                'socials' => $socials,
                'contact_address' => $contact_address,
                'header_image' => $header_image,
                'mail_logo' => $mail_logo,
                'empty_layout' => $empty_layout
            ]);


        //TODO Need to be in config file
        if (getenv('ENVIRONMENT') === 'development' || getenv('ENVIRONMENT') === 'local') {
            $this->CI->load->library('email');

            $config = $this->CI->config->item('mailtrap_config');

            $this->CI->email->initialize($config);
            $this->CI->email->from($sender_email, $sender_name);
            $this->CI->email->to($recipients);

            $this->CI->email->subject($subject);
            $this->CI->email->message($html_part);
            $this->CI->email->set_mailtype("html");

            return $this->CI->email->send();

        } else {
            // We prepare the recipients, to be work with the API
            $array_recipients = array();
            foreach ($recipients as $recipient) {
                $array_recipients[] = ['Email' => $recipient];
            }

            $mailjet = new \Mailjet\Client($this->CI->config->item('mailjet_public_key'), $this->CI->config->item('mailjet_secret_key'), true, ['version' => 'v3.1', 'secured'=> true]);

            //$mailjet->debug = 0;
            $body = [
                'Messages' => [
                    [
                        'From' => [
                            'Email' => $this->CI->config->item('mailjet_sender_address'),
                            'Name' => "Ystos"
                        ],
                        'To' => [
                            [
                                'Email' => $recipient,
                            ]
                        ],
                        'Subject' => $subject,
                        'HTMLPart' => $html_part
                    ]
                ]
            ];
            $response = $mailjet->post(Resources::$Email, ['body' => $body]);
            $response->success();

            return  $response->success();
        }


    }
}