<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 13/08/2017
 * Time: 09:54
 */
if (!function_exists('upload_image'))
{
    function upload_image($user_id)
    {
        $CI =& get_instance();

        $config['upload_path'] = $CI->config->item('base_path').'assets'.DIRECTORY_SEPARATOR.'medias'.DIRECTORY_SEPARATOR.'classifieds'.DIRECTORY_SEPARATOR.'tmp';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '5120';
        $config['encrypt_name'] = false;
        $config['file_name'] = mt_rand(0,1000)."_".date("YmdHis")."_".$user_id;

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload("picture_file")) {
            $response = array(
                'error' => true,
                'message' => $CI->upload->display_errors(null, null)
            );
            return $response;
        }
        else {
            $upload_response = $CI->upload->data();
            $CI->load->library('image_autorotate', array('filepath' => $upload_response['full_path']));
            $response = array(
                'error' => false,
                'data' => array(
                    'name' => $upload_response['file_name']
                )
            );
        }

        return $response;
    }
}

if (!function_exists('store_image'))
{
    function store_image( $user_id, $path = 'media/classifieds/tmp')
    {
        $CI =& get_instance();

        if(!file_exists($CI->config->item('base_path').'/assets/'.$path)){
            mkdir($CI->config->item('base_path').'/assets/'.$path,0777,TRUE);
            mkdir($CI->config->item('base_path').'/assets/'.$path.'/thumbnail',0777,TRUE);
        }

        $config['upload_path'] = $CI->config->item('base_path').'/assets/'.$path;
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '5120';
        $config['encrypt_name'] = false;
        $config['file_name'] = mt_rand(0,1000)."_".date("YmdHis")."_".$user_id.'.png';

        $CI->load->library('upload', $config);

        if (!$CI->upload->do_upload("picture_file")) {
            $response = array(
                'error' => true,
                'message' => $CI->upload->display_errors(null, null)
            );
            return $response;
        }
        else {
            $upload_response = $CI->upload->data();
            $CI->load->library('image_autorotate', array('filepath' => $upload_response['full_path']));

            $CI->load->library('image_lib');

            $img_cfg['image_library'] = 'gd2';
            $img_cfg['source_image'] = $CI->config->item('public_assets_path').$path.'/'.$upload_response['file_name'];
            $img_cfg['maintain_ratio'] = TRUE;
            $img_cfg['create_thumb'] = TRUE;
            $img_cfg['thumb_marker'] = '';
            $img_cfg['new_image'] = $CI->config->item('public_assets_path').$path.'/thumbnail/'.$upload_response['file_name'];
            $img_cfg['width'] = 400;
            $img_cfg['quality'] = 80;
            $img_cfg['height'] = 400;

            $CI->image_lib->initialize($img_cfg);
            $CI->image_lib->resize();

            $response = array(
                'error' => false,
                'data' => array(
                    'name' => $upload_response['file_name']
                )
            );
        }

        return $response;
    }
}
if (!function_exists('rotate_image'))
{
    function rotate_image($path, $angle)
    {
        $CI =& get_instance();

        $CI->load->library('image_lib');

        $config['image_library'] = 'gd2';
        $config['source_image']	= $path;

        if($angle === '-90'){
            $ori = '270';
        }
        else{
            $ori = $angle;
        }

        $config['rotation_angle'] = $ori;
        $CI->image_lib->initialize($config);

        if ( ! $CI->image_lib->rotate()){
            $response = array(
                'success' => FALSE,
                'message' => $CI->image_lib->display_errors(null, null)
            );
        }
        else{
            $response = array(
                'success' => true
            );
        }

        return $response;
    }
}