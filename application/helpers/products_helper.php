<?php
/**
 * Created by PhpStorm.
 * User: Etienne
 * Date: 01/11/2017
 * Time: 16:45
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('is_mine'))
{
    function is_mine(\Ystos\Common\Entity\Product\Product $classified)
    {
        $CI =& get_instance();
        if ($CI->session->has_userdata('logged_user')) {
            /**
             * @var $user \Ystos\Common\Entity\User
             */
            $user_tmp = unserialize($CI->session->userdata('logged_user'));

            $CI->load->library('doctrine');

            /**
             * @var $em \Doctrine\ORM\EntityManager
             */
            $em = $CI->doctrine->em;
            /**
             * @var $user_repository \Ystos\Common\Repository\User_Repository
             */
            $user_repository = $em->getRepository('Ystos\Common\Entity\User');

            if (is_array($user_tmp)) {
                $id = $user_tmp['id'];
            } else {
                $id = $user_tmp->getId();#
            }

            return $classified->getOwner()->getId() == $id;
        }

        return false;
    }
}


if (!function_exists('get_product_link'))
{
    function get_product_link(\Ystos\Common\Entity\Product\Product $product)
    {
        return 'classified/'.$product->getCategory()->getSlug().'/'.$product->getId();
    }
}