<?php
/**
 * Created by PhpStorm.
 * User: piche
 * Date: 28/06/2017
 * Time: 17:37
 */

namespace Ystos\Common\Service;

use Ystos\Common\Entity\Notification\Config;
use Ystos\Common\Entity\Product\Product;
use Ystos\Common\Entity\User;

require_once COMMONPATH.'models/Entity/Notification/Config.php';
require_once COMMONPATH.'models/Entity/Notification/Notification.php';



class Notification
{

    protected $CI;

    protected $redis;

    /**
     * @var $em \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var $pusher \Pusher
     */
    protected $pusher;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->library('doctrine');

        $this->em = $this->CI->doctrine->em;

        $this->public_url = $this->CI->config->item('base_public_url');

        $options = array(
            'cluster' => 'eu',
            'encrypted' => true
        );
        $this->pusher = new \Pusher(
            '9533501ba4eb857af07f',
            '121ee12ae7ac484feef6',
            '390667',
            $options
        );


    }

    public function add_new_offer_notification(User $seller,User $owner)
    {
        $notification = new \Ystos\Common\Entity\Notification();
        $notification->setUser($owner);
        $notification->setContent('Vous avez une nouvelle offre pour un de vos produits');
        $notification->setUrl($this->public_url.'account/me');
        $notification->setIcon($this->public_url."assets/medias/pictures/".$seller->getPicture());
        $notification->setType('offer');

        $this->em->persist($notification);
        $this->em->flush();

        $this->send_real_time_user_notification($notification, 'new-offer');
        $this->increment_notification_user($owner);

    }

    public function add_new_message_notification(User $sender,User $receiver)
    {
        $notification = new \Ystos\Common\Entity\Notification();
        $notification->setUser($receiver);
        $notification->setContent('Vous avez un nouveau message');
        $notification->setUrl($this->public_url.'account/messaging');
        $notification->setIcon($this->public_url. "assets/medias/pictures/".$sender->getPicture());
        $notification->setType('messages');

        $this->em->persist($notification);
        $this->em->flush();

        $this->send_real_time_user_notification($notification, 'new-message');
        $this->increment_notification_user($receiver);
    }

    public function notify_order_cancel(User $receiver, $product_title)
    {
        $notification = new \Ystos\Common\Entity\Notification();
        $notification->setUser($receiver);
        $notification->setContent($product_title.': L\'offre a été annulée');
        $notification->setUrl($this->public_url.'account/me');
        $notification->setType('order_canceled');

        $this->em->persist($notification);
        $this->em->flush();

        $this->send_real_time_user_notification($notification, 'order-canceled');
        $this->increment_notification_user($receiver);
    }

    public function notify_new_order(User $receiver, Product $product)
    {
        $notification = new \Ystos\Common\Entity\Notification();
        $notification->setUser($receiver);
        $notification->setContent('Votre '.$product->getTitle().' a été vendu');
        $notification->setUrl($this->public_url.'sell/'.$product->getId());
        $notification->setIcon($this->public_url."assets/medias/classifieds/".$product->getPictures()[0]);
        $notification->setType('order_created_'.$product->getId());

        $this->em->persist($notification);
        $this->em->flush();

        $this->send_real_time_user_notification($notification, 'order-created');
        $this->increment_notification_user($receiver);
    }



    private function send_real_time_user_notification(\Ystos\Common\Entity\Notification $notification, $event_name){
        $data['message'] = $notification->getContent();
        $data['url'] = $notification->getUrl();
        $data['icon'] = $notification->getIcon();

        /**
         * @var $notification_config Config
         */
        $notification_config = $this->em->getRepository('Ystos\Common\Entity\Notification\Config')->findOneBy(array('user' => $notification->getUser()));

        $this->pusher->trigger($notification_config, 'ystos-action-event', $data);
    }

    public function remove_type_notifications($type, $user_id){
        $notifications = $this->em->getRepository('Ystos\Common\Entity\Notification')->findBy(array('user' => $user_id, 'type' => $type, 'viewed'=>FALSE));

        foreach ($notifications as $notification){
            /**
             * @var $notification \Ystos\Common\Entity\Notification
             */
            $notification->setViewed(TRUE);
            $this->em->persist($notification);
            $this->em->flush();
            $this->decrement_notification_user($user_id);
        }
    }

    public function increment_notification_user(User $user){
        $this->redis = new \Predis\Client([
            'host' => $this->CI->config->item('redis_url'),
        ]);

        $this->redis->hincrby("notifications", $user->getId(), 1);
    }

    public function decrement_notification_user($user_id){
        $this->redis = new \Predis\Client([
            'host' => $this->CI->config->item('redis_url'),
        ]);

        $this->redis->hincrby("notifications", $user_id, -1);
    }

    public function count_notifications($user_id){
        $this->redis = new \Predis\Client([
            'host' => $this->CI->config->item('redis_url'),
        ]);
        return $this->redis->hget("notifications", $user_id);
    }


}