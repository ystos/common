# Contributing to Ystos Automated Tasks

## Guidelines

### PHP Style

All code must meet the [Style Guide](https://codeigniter.com/user_guide/general/styleguide.html), which is
essentially the [Allman indent style](https://en.wikipedia.org/wiki/Indent_style#Allman_style), underscores and readable operators. This makes certain that all code is the same format as the existing code and means it will be as readable as possible.

### Documentation

If you change anything that requires a change to documentation then you will need to add it. New classes, methods, parameters, changing default values, etc are all things that will require a change to documentation. The change-log must also be updated for every change. Also PHPDoc blocks must be maintained.

### Compatibility

Ystos AT recommends PHP 7.0 or newer.
