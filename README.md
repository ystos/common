# Ystos Common


Ystos Common regroup the common components like the models and service classes



## How to use it

For now, add this line in the composer.json file

```cmd
  "require": {
    "ystos/common": "@dev"
  },
  "repositories": [
    {
      "type": "path",
      "url": "../common"
    }
  ]
```

Then you can load your classes in the app 